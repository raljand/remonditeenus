<%@page import="view.ViewLib"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:useBean scope="request" id="view" type="view.View"></jsp:useBean>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css"
	href="/t103728_remonditeenus/static/css/base.css">
</head>
<body>
	<ul>
		<li><a href="/t103728_remonditeenus/service/request">Service requests</a></li>
		<li><a href="/t103728_remonditeenus/service/request/register">Register
				service request</a></li>
		<li><a href="/t103728_remonditeenus/order">Service orders</a></li>
		<li><a href="//imbi.ld.ttu.ee/tomcat_webapp_logs/t103728_remonditeenus/log.txt">log file</a>
		<li><form action="/t103728_remonditeenus/logout" method="post"><input type="submit" value="Log out"></form>
	</ul>
	<%
		out.print(ViewLib.render(view));
	%>
</body>
</html>