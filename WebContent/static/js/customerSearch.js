function getParent(tag) {
	return $(tag).parents(".js-finder-parent");
}

function searchCustomer(button) {
	var parent = getParent(button);
	var searchDiv = parent.find('.js-finder');
	var search = searchDiv.find('input[type=search]');

	var populateList = function() {
		$.ajax({
			url : base.urlRoot + '/customer/search',
			data : {
				term : search.val()
			},
			success : function(data) {
				var ul = searchDiv.find('ul');
				var i;

				ul.empty();
				for (i = 0; i < data.length; i++) {
					ul.append('<li><a onclick="selectCustomer(' + data[i].key
							+ ', \'' + data[i].name + '\', this)">'
							+ data[i].name + "</a></li>");
				}
			}
		});
	};

	populateList();
	searchDiv.removeClass("is-hidden");
	search.on('input', populateList);
}

function selectCustomer(key, name, link) {
	var parent = base.parents($(link), "js-finder-parent");

	parent.find('input[type=hidden]').val(key);
	parent.find(".js-finder-slug").text(name);
	parent.find(".js-finder").addClass("is-hidden");
}
