var devices = Devices();

function Devices() {
	var Dom = {
		span : function(text) {
			return $("<span></span>").text(text);
		},
		input : function(name, type) {
			return $("<input required name='" + name + "' type='" + type
					+ "' />");
		},
		inputStr : function(name, type) {
			return "<input required name='" + name + "' type='" + type + "' />";
		},
		td : function() {
			return $("<td></td>").append(Array.prototype.slice.call(arguments));
		}
	};

	var classes = {
		data : "js-data",
		sum : "js-devices-sum",
		amount : "js-devices-amount",
		price : "js-devices-price",
		row : "js-devices-row",
		table : "js-devices-table"
	};

	function find(wrapper, source, target) {
		return base.find(base.parents($(source), wrapper), target);
	}

	function calcSum() {
		var amount = find(classes.row, this, classes.amount).val();
		var price = find(classes.row, this, classes.price).val();
		if (!(isNaN(amount) || isNaN(price))) {
			find(classes.row, this, classes.sum).val(amount * price);
		}
	}

	$("." + classes.amount).on("input", calcSum);
	$("." + classes.price).on("input", calcSum);

	function addRow(button, header, extra_content, units, names, type) {
		find(classes.data, button, classes.table).append(
				$("<tr></tr>").addClass(classes.row).append(
						$("<th></th>").text(header),
						Dom.td(Dom.input(names[0], "hidden").val(type), Dom
								.inputStr(names[1], "text"), extra_content),
						Dom.td(Dom.span("kogus:"), Dom
								.input(names[2], "number").addClass(
										"number-input " + classes.amount).on(
										'input', calcSum), Dom.span("[" + units
								+ "]")),
						Dom.td(Dom.span("ühiku hind:"), Dom.input(names[3],
								"number").addClass(
								"number-input " + classes.price).on('input',
								calcSum)),
						Dom.td(Dom.span("hind kokku:"), Dom.input("", "number")
								.attr("disabled", "").addClass(
										"number-input " + classes.sum))));
	}

	return {
		getOptionText : function(button) {
			var data = $(button).parents(".js-data");
			var option = data.find(".js-device :selected");
			return option.val() + " " + option.text();
		},
		newService : function(button, names) {
			addRow(button, "töö:", "teenus:<select>"
					+ "<option>raske remont test</option></select>", "tundi",
					names, "action");
		},
		newPart : function(button, names) {
			addRow(button, "osa:", "", "tk.", names, "part");
		},
		calcSum : calcSum
	};
}
