var base = Base();

function Base() {
	function check(jquery, class_) {
		if (!jquery.length) {
			throw "Jquery set shouldn't be empty: " + class_;
		}
		return jquery;
	}

	return {
		ignoreEnter : function(e) {
			if (e.keyCode == '13') {
				return false;
			}
			return true;
		},
		parents : function(jquery, class_) {
			return check(jquery.parents("." + class_), class_);
		},
		find : function(jquery, class_) {
			return check(jquery.find("." + class_), class_);
		},
		urlRoot : "/t103728_remonditeenus"
	};
};