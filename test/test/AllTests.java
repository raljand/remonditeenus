package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import view.RenderLibTest;

@RunWith(Suite.class)
@SuiteClasses({ RenderLibTest.class })
public class AllTests {

}
