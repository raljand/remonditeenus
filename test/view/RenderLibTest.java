package view;

import org.junit.Test;

import view.field.Field;
import view.field.Name;
import view.field.model.FieldPair;
import view.field.model.Translator;
import view.field.rendering.Renderer;
import view.field.rendering.Tag;
import db.Typed;

public class RenderLibTest {

	@Test
	public void renderFieldPairTest() {
		RenderLib.render(new FieldPair<>(new Field<>(new Renderer<Object>() {
			@Override
			public Tag render(Object value, Name name) {
				return null;
			}
		}, null, null, null), null, new Translator<Object, Object>() {
			@Override
			public Object renderable(Typed<Object> value) {
				return null;
			}

			@Override
			public Object saveable(Object value) {
				return null;
			}
		}, null), null);
	}
}
