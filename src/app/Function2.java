package app;

public interface Function2<F, G, T> {
	T apply(F arg0, G arg1);
}
