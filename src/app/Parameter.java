package app;

public class Parameter {

	public final String value;
	public final String name;

	public Parameter(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public static Parameter class_(String class_) {
		return new Parameter("class", class_);
	}

	public static Parameter defer() {
		return new Parameter("defer", "");
	}

	public static Parameter method(String method) {
		return new Parameter("method", method);
	}

	public static Parameter name(String name) {
		return new Parameter("name", name);
	}

	public static Parameter onclick(String event) {
		return new Parameter("onclick", event);
	}

	public static Parameter onkeydown(String event) {
		return new Parameter("onkeydown", event);
	}

	public static Parameter placeholder(String placeholder) {
		return new Parameter("placeholder", placeholder);
	}

	public static Parameter type(String type) {
		return new Parameter("type", type);
	}

	public static Parameter value(String value) {
		return new Parameter("value", value);
	}

	public static Parameter colspan(int i) {
		return new Parameter("colspan", Integer.toString(i));
	}

	public static Parameter multiple() {
		return new Parameter("multiple", "");
	}

	public static Parameter selected() {
		return new Parameter("selected", "");
	}

}
