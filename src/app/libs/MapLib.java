package app.libs;

import java.util.HashMap;
import java.util.Map;

import com.google.common.base.Function;

public class MapLib {

	public static <F, T> Map<F, T> map(Function<F, T> function,
			Iterable<? extends F> items) {
		Map<F, T> result = new HashMap<>();
		for (F item : items) {
			result.put(item, function.apply(item));
		}
		return result;
	}

	private MapLib() {
	}
}
