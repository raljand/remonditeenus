package app.libs;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import view.field.rendering.Tag;
import app.FoldFunction;
import app.Parameter;
import app.Settings;

import com.google.common.base.Function;

public class StringLib {

	public static String safeProduct(Integer value1, Integer value2) {
		return value1 == null || value2 == null ? "" : String.valueOf(value1
				* value2);
	}

	public static String safeStr(Object value) {
		return value == null ? "" : value.toString();
	}

	public static String jsArray(List<String> items) {
		return "[" + join(null, ListLib.map(new Function<String, String>() {
			@Override
			public String apply(String arg0) {
				return jsString(arg0);
			}
		}, items), ", ") + "]";
	}

	public static String jsString(String value) {
		return "'" + value + "'";
	}

	public static String tag(Tag tag) {
		return "<"
				+ tag.name
				+ " "
				+ StringLib.attributes(tag.params)
				+ (tag.content == null ? "/"
						: (">\n" + tag.content + "</" + tag.name)) + ">";
	}

	public static String tags(final List<Tag> tags) {
		return StringLib.join(new Function<Tag, String>() {
			@Override
			public String apply(Tag arg0) {
				return arg0 == null ? null : StringLib.tag(arg0);
			}
		}, tags);
	}

	public static String normalisedUrl(String url) {
		return normUrl(url, Arrays.<Parameter> asList());
	}

	public static String normUrl(String url, List<Parameter> params) {
		return (url.startsWith(Settings.URL_ROOT) || isUrlRelative(url) ? ""
				: Settings.URL_ROOT)
				+ url
				+ (params.size() == 0 ? "" : "?" + str(params, "&", ""));
	}

	public static boolean isUrlRelative(String url) {
		return !url.startsWith("/");
	}

	public static String attributes(List<Parameter> params) {
		return str(params, " ", "\"");
	}

	private static String str(List<Parameter> params, String separator,
			final String wrapper) {
		return StringLib.join(new Function<Parameter, String>() {
			@Override
			public String apply(Parameter arg0) {
				return arg0.value == null ? null : (arg0.name + "=" + wrapper
						+ arg0.value + wrapper);
			}
		}, params, separator);
	}

	public static <T> String join(Function<T, String> function,
			Collection<T> items) {
		return join(function, items, "");
	}

	public static <T> String join(final Function<T, String> function,
			Collection<T> items, final String separator) {
		String result = StdLib.fold(new FoldFunction<T, String>() {
			@Override
			public String call(T arg0, String arg1) {
				String item = (function == null ? (arg0 == null ? null : arg0
						.toString()) : function.apply(arg0));
				return arg1 + (item == null ? "" : (separator + item));
			}
		}, items, "");
		return result.length() == 0 ? result : result.substring(separator
				.length());
	}

	private StringLib() {
	}
}
