package app.libs;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import view.field.model.ActionLink;
import view.field.rendering.ConverterLib;
import app.Parameter;

import com.google.common.base.Function;

import controller.RequestLib;
import controller.servlet.service.CreateOrderServlet;
import db.Typed;
import db.model.ServiceRequest;

public final class FunctionLib {

	public static Function<Void, String> constant(final String string) {
		return new Function<Void, String>() {
			@Override
			public String apply(Void arg0) {
				return string;
			}
		};
	}

	public static Function<Void, String> actionLink(
			final HttpServletRequest req, final String idParam,
			final String name) {
		return new Function<Void, String>() {
			@Override
			public String apply(Void arg0) {
				return StringLib.normUrl("", Arrays.asList(
						new Parameter(idParam, Integer.toString(RequestLib
								.number(req, idParam))), new Parameter(
								ActionLink.KEYWORD, name)));
			}
		};
	}

	private FunctionLib() {
	}

	public static Function<Void, String> orderRegistration(
			final ServiceRequest order) {
		return new Function<Void, String>() {
			@Override
			public String apply(Void arg0) {
				return CreateOrderServlet.URL
						+ StringLib.normUrl("", Arrays.asList(new Parameter(
								CreateOrderServlet.REQUEST_PARAM, ConverterLib
										.<Integer> str()
										.apply(Typed
												.integer(order == null ? null
														: order.getId())))));
			}
		};
	}
}
