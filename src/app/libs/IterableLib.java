package app.libs;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import view.field.Name;

import com.google.common.base.Function;

public class IterableLib {

	private IterableLib() {
	}

	public static Iterable<Map<String, String>> paramValues(
			final HttpServletRequest req, final Name name) {
		return new Iterable<Map<String, String>>() {
			@Override
			public Iterator<Map<String, String>> iterator() {
				final List<String> names = ListLib.names(name);
				final Map<String, Iterator<String>> iters = MapLib.map(
						new Function<String, Iterator<String>>() {
							@Override
							public Iterator<String> apply(String arg0) {
								String[] values = req.getParameterValues(arg0);
								return IteratorLib
										.iterator(values == null ? new String[0]
												: values);
							}
						}, names);
				return new Iterator<Map<String, String>>() {
					@Override
					public void remove() {
						throw new UnsupportedOperationException();
					}

					@Override
					public Map<String, String> next() {
						return MapLib.map(new Function<String, String>() {
							@Override
							public String apply(String name) {
								return iters.get(name).next();
							}
						}, names);
					}

					@Override
					public boolean hasNext() {
						return names.size() == 0 ? false : iters.get(
								names.get(0)).hasNext();
					}
				};
			}
		};
	}

}
