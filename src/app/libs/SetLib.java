package app.libs;

import java.util.Collection;
import java.util.Set;

import app.NullLessSet;

import com.google.common.base.Function;

public class SetLib {

	public static <F, T> Set<T> map(Function<F, ? extends T> fun,
			Iterable<? extends F> items) {
		return map(new NullLessSet<T>(), fun, items);
	}

	static <F, T, U extends Collection<T>> U map(U collection,
			Function<F, ? extends T> fun, Iterable<? extends F> items) {
		for (F item : items) {
			T resultItem = fun.apply(item);
			collection.add(resultItem);
		}
		return collection;
	}

	private SetLib() {
	}
}
