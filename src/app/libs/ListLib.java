package app.libs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import view.field.HasError;
import view.field.Name;
import app.FoldFunction;
import app.Function2;

import com.google.common.base.Function;

public class ListLib {

	@Deprecated
	public static <F, T> HasError<List<T>> errorMap(
			Function<F, ? extends HasError<? extends T>> fun,
			Iterable<? extends F> items) {
		List<T> result = new LinkedList<T>();
		boolean hasError = false;
		for (F item : items) {
			HasError<? extends T> resultItem = fun.apply(item);
			hasError = resultItem.hasError ? true : hasError;
			result.add(resultItem.value);
		}
		return new HasError<List<T>>(result, hasError);
	}

	public static List<String> names(final Name name) {
		return map(new Function<String, String>() {
			@Override
			public String apply(String arg0) {
				return name.name + "_" + arg0;
			}
		}, name.subNames);
	}

	public static <T> List<T> append(List<? extends T> list1,
			List<? extends T> list2) {
		List<T> concatted = new LinkedList<T>();
		concatted.addAll(list1);
		concatted.addAll(list2);
		return concatted;
	}

	public static <T> List<T> flatten(List<List<T>> items) {
		return StdLib.fold(new FoldFunction<List<T>, List<T>>() {
			@Override
			public List<T> call(List<T> arg0, List<T> arg1) {
				arg1.addAll(arg0);
				return arg1;
			}
		}, items, new LinkedList<T>());
	}

	public static <F, T> List<T> map(Function<F, T> function,
			Iterable<? extends F> items) {
		List<T> result = new LinkedList<>();
		for (F item : items) {
			result.add(function.apply(item));
		}
		return result;
	}

	public static <F, E, T> List<T> map(Function2<F, E, T> function,
			List<? extends F> items, List<? extends E> items2) {
		List<T> result = new ArrayList<>(items.size() * 2);
		for (int i = 0; i < items.size(); ++i) {
			result.add(function.apply(items.get(i),
					items2 != null ? items2.get(i) : null));
		}
		return result;
	}

	private ListLib() {
	}
}
