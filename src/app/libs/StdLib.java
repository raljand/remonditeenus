package app.libs;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import app.FoldFunction;

import com.google.common.base.Function;

public class StdLib {

	public static <F, T> List<T> expand(Function<F, T> function,
			Function<F, T> function2, Collection<F> items) {
		List<T> results = new LinkedList<>();
		for (F item : items) {
			results.add(function.apply(item));
			results.add(function2.apply(item));
		}
		return results;
	}

	public static <T> List<T> removeNull(List<T> items) {
		return filter(new Function<T, Boolean>() {
			@Override
			public Boolean apply(T arg0) {
				return arg0 != null;
			}
		}, items);
	}

	public static <T> List<T> filter(Function<T, Boolean> function,
			List<T> items) {
		List<T> result = new LinkedList<>();
		for (T item : items) {
			if (function.apply(item)) {
				result.add(item);
			}
		}
		return result;
	}

	public static <T> Class<Set<T>> classOfSet() {
		return (Class<Set<T>>) (Class<?>) Set.class;
	}

	public static <T> T call(Callable<T> callable) {
		try {
			return callable.call();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static <F, T> T fold(FoldFunction<F, T> function,
			Iterable<? extends F> items, T init) {
		T result = init;
		for (F item : items) {
			result = function.call(item, result);
		}
		return result;
	}

	@SafeVarargs
	public static <F, T> T fold(FoldFunction<F, T> function, T init, F... items) {
		T result = init;
		for (F item : items) {
			result = function.call(item, result);
		}
		return result;
	}

	private StdLib() {
	}
}
