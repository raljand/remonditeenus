package app.libs;

import java.util.Iterator;

public class IteratorLib {

	public static <T> Iterator<T> iterator(final T[] array) {
		return new Iterator<T>() {
			private int i = 0;

			@Override
			public boolean hasNext() {
				return i < array.length;
			}

			@Override
			public T next() {
				return array[i++];
			}

			@Override
			public void remove() {
				array[i] = null;
			}
		};
	}
}
