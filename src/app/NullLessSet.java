package app;

import java.util.HashSet;

public class NullLessSet<E> extends HashSet<E> {

	private static final long serialVersionUID = 1L;

	@Override
	public boolean add(E e) {
		if (e == null) {
			throw new IllegalArgumentException("Item should not be null");
		} else {
			return super.add(e);
		}
	}
}
