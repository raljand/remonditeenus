package app;

public interface Function3<F, G, H, T> {
	T apply(F arg0, G arg1, H arg2);
}
