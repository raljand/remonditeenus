package app;

public interface FoldFunction<F, T> {
	T call(F item, T result);
}
