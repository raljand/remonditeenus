package db;

import db.model.ServiceDevice;
import db.model.ServiceOrder;

public interface Service {

	ServiceDevice getService_device_fk();

	void setService_order_fk(ServiceOrder order);

	ServiceOrder getService_order_fk();

	String getDescription();

	void setDescription(String description);

	Integer getAmount();

	void setAmount(Integer amount);

	Integer getPrice();

	void setPrice(Integer price);
}
