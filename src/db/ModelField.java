package db;

import java.util.Set;

import app.libs.StdLib;
import db.model.Employee;
import db.model.SOStatusType;
import db.model.ServiceDevice;
import db.model.ServiceRequest;

public class ModelField<T> {

	public final Class<T> type;
	public final String name;

	public ModelField(Class<T> type, String name) {
		this.type = type;
		this.name = name;
	}

	public static ModelField<Employee> employee() {
		return new ModelField<>(Employee.class, "createdBy");
	}

	public static ModelField<ServiceRequest> serviceRequest() {
		return new ModelField<>(ServiceRequest.class, "service_request_fk");
	}

	public static ModelField<Set<Service>> serviceSet() {
		return set("services");
	}

	public static ModelField<SOStatusType> soStatusType() {
		return new ModelField<>(SOStatusType.class, "so_status_type_fk");
	}

	public static ModelField<Set<ServiceDevice>> devices() {
		return set("devices");
	}

	public static <T> ModelField<Set<T>> set(String name) {
		return new ModelField<>(StdLib.<T> classOfSet(), name);
	}

	public static ModelField<String> string(String name) {
		return new ModelField<>(String.class, name);
	}

}
