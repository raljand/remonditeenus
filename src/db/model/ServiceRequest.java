package db.model;

import java.util.Date;

import db.Id;
import db.Slug;

public class ServiceRequest {

	private Integer id;
	private ServiceRequestStatusType status;
	private Customer customer;
	private Employee createdBy;
	private Date created;
	private String serviceDescByCustomer;
	private String serviceDescByEmployee;

	@Id
	@Slug
	public Integer getId() {
		return id;
	}

	@SuppressWarnings("unused")
	private void setId(int id) {
		this.id = id;
	}

	public ServiceRequestStatusType getStatus() {
		return status;
	}

	public void setStatus(ServiceRequestStatusType status) {
		this.status = status;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Employee getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Employee createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getServiceDescByCustomer() {
		return serviceDescByCustomer;
	}

	public void setServiceDescByCustomer(String serviceDescByCustomer) {
		this.serviceDescByCustomer = serviceDescByCustomer;
	}

	public String getServiceDescByEmployee() {
		return serviceDescByEmployee;
	}

	public void setServiceDescByEmployee(String serviceDescByEmployee) {
		this.serviceDescByEmployee = serviceDescByEmployee;
	}
}
