package db.model;

import db.Id;
import db.Slug;
import db.dao.DaoLib;
import db.dao.SubjectDAO;

public class Customer {

	private int id;
	private SubjectType subjectType;
	private int subjectTypeKey;
	private Subject subject;
	private int subjectKey;

	@Id
	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public SubjectType getSubjectType() {
		if (subjectType == null) {
			subjectType = DaoLib.load(SubjectType.class, subjectTypeKey);
		}
		return subjectType;
	}

	public void setSubjectType(SubjectType subjectType) {
		this.subjectType = subjectType;
	}

	public int getSubjectTypeKey() {
		return subjectTypeKey;
	}

	public void setSubjectTypeKey(int key) {
		subjectTypeKey = key;
	}

	public Subject getSubject() {
		if (subject == null) {
			subject = SubjectDAO.getSubject(getSubjectKey(),
			        getSubjectTypeKey());
		}
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public int getSubjectKey() {
		return subjectKey;
	}

	public void setSubjectKey(int key) {
		subjectKey = key;
	}

	@Slug
	public String getSubjectName() {
		return getSubject().getName();
	}
}
