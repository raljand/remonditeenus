package db.model;

import db.Service;

public class ServiceAction implements Service {

	private Integer id;
	private ServiceDevice service_device_fk;
	private ServiceOrder service_order_fk;
	private Integer service_amount;
	private Integer price;
	private String action_description;

	@Override
	public Integer getAmount() {
		return getService_amount();
	}

	@Override
	public void setAmount(Integer amount) {
		setService_amount(amount);
	}

	@Override
	public String getDescription() {
		return getAction_description();
	}

	@Override
	public void setDescription(String description) {
		setAction_description(description);
	}

	public Integer getId() {
		return id;
	}

	@SuppressWarnings("unused")
	private void setId(Integer id) {
		this.id = id;
	}

	public ServiceDevice getService_device_fk() {
		return service_device_fk;
	}

	public void setService_device_fk(ServiceDevice service_device_fk) {
		this.service_device_fk = service_device_fk;
	}

	public ServiceOrder getService_order_fk() {
		return service_order_fk;
	}

	public void setService_order_fk(ServiceOrder service_order_fk) {
		this.service_order_fk = service_order_fk;
	}

	public Integer getService_amount() {
		return service_amount;
	}

	public void setService_amount(Integer service_amount) {
		this.service_amount = service_amount;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getAction_description() {
		return action_description;
	}

	public void setAction_description(String action_description) {
		this.action_description = action_description;
	}
}
