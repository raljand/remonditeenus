package db.model;

public class ServiceDevice {

	private Integer id;
	private Device device_fk;
	private ServiceOrder service_order_fk;
	private ServiceAction serviceAction;
	private ServicePart servicePart;

	public Integer getId() {
		return id;
	}

	@SuppressWarnings("unused")
	private void setId(Integer id) {
		this.id = id;
	}

	public Device getDevice_fk() {
		return device_fk;
	}

	public void setDevice_fk(Device device_fk) {
		this.device_fk = device_fk;
	}

	public ServiceOrder getService_order_fk() {
		return service_order_fk;
	}

	public void setService_order_fk(ServiceOrder service_order_fk) {
		this.service_order_fk = service_order_fk;
	}

	public ServiceAction getServiceAction() {
		return serviceAction;
	}

	public void setServiceAction(ServiceAction serviceAction) {
		this.serviceAction = serviceAction;
	}

	public ServicePart getServicePart() {
		return servicePart;
	}

	public void setServicePart(ServicePart servicePart) {
		this.servicePart = servicePart;
	}

}
