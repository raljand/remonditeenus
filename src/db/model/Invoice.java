package db.model;

import java.util.Date;

public class Invoice {

	private int id;
	private Customer customer;
	private InvoiceStatusType status;
	private ServiceOrder serviceOrder;
	private Date date;
	private Date dueDate;
	private int priceTotal;
	private String receiverName;
	private int referenceNumber;
	private String receiverAccounts;

	public int getId() {
		return id;
	}

	@SuppressWarnings("unused")
	private void setId(int id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public InvoiceStatusType getStatus() {
		return status;
	}

	public void setStatus(InvoiceStatusType status) {
		this.status = status;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public int getPriceTotal() {
		return priceTotal;
	}

	public void setPriceTotal(int priceTotal) {
		this.priceTotal = priceTotal;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public int getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(int referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getReceiverAccounts() {
		return receiverAccounts;
	}

	public void setReceiverAccounts(String receiverAccounts) {
		this.receiverAccounts = receiverAccounts;
	}
}
