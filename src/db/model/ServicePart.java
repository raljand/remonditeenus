package db.model;

import db.Service;

public class ServicePart implements Service {

	private Integer id;
	private ServiceDevice service_device_fk;
	private ServiceOrder service_order_fk;
	private String part_name;
	private Integer part_price;
	private Integer part_count;

	@Override
	public String getDescription() {
		return getPart_name();
	}

	@Override
	public void setDescription(String description) {
		setPart_name(description);
	}

	@Override
	public Integer getAmount() {
		return getPart_count();
	}

	@Override
	public void setAmount(Integer amount) {
		setPart_count(amount);
	}

	@Override
	public Integer getPrice() {
		return getPart_price();
	}

	@Override
	public void setPrice(Integer price) {
		setPart_price(price);
	}

	public Integer getId() {
		return id;
	}

	@SuppressWarnings("unused")
	private void setId(Integer id) {
		this.id = id;
	}

	public ServiceDevice getService_device_fk() {
		return service_device_fk;
	}

	public void setService_device_fk(ServiceDevice service_device_fk) {
		this.service_device_fk = service_device_fk;
	}

	public ServiceOrder getService_order_fk() {
		return service_order_fk;
	}

	public void setService_order_fk(ServiceOrder service_order_fk) {
		this.service_order_fk = service_order_fk;
	}

	public String getPart_name() {
		return part_name;
	}

	public void setPart_name(String part_name) {
		this.part_name = part_name;
	}

	public Integer getPart_price() {
		return part_price;
	}

	public void setPart_price(Integer part_price) {
		this.part_price = part_price;
	}

	public Integer getPart_count() {
		return part_count;
	}

	public void setPart_count(Integer part_count) {
		this.part_count = part_count;
	}
}
