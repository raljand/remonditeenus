package db.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.commons.lang3.StringUtils;

public class ReflectionLib {
	@SuppressWarnings("unchecked")
	public static <T> T getValue(Object model, String fieldName) {
		if (model == null) {
			return null;
		}

		try {
			String methodName = "get" + StringUtils.capitalize(fieldName);
			Method method = model.getClass().getMethod(methodName);
			Object value = method.invoke(model);
			return (T) value;
		} catch (NoSuchMethodException | SecurityException
				| IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}
}
