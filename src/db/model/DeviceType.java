package db.model;

public class DeviceType {
	private int id;
	private DeviceType superType;
	private String typeName;
	private int level;
	public int getId() {
		return id;
	}
	@SuppressWarnings("unused")
	private void setId(int id) {
		this.id = id;
	}
	public DeviceType getSuperType() {
		return superType;
	}
	public void setSuperType(DeviceType superType) {
		this.superType = superType;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
}
