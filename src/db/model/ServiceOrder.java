package db.model;

import java.util.HashSet;
import java.util.Set;

import db.Id;
import db.Service;

public class ServiceOrder {

	private Integer id;
	private SOStatusType so_status_type_fk;
	private ServiceRequest service_request_fk;
	private String note;

	private Set<ServiceDevice> devices = new HashSet<>();
	private Set<ServiceAction> actions = new HashSet<>();
	private Set<ServicePart> parts = new HashSet<>();

	@Id
	public Integer getId() {
		return id;
	}

	@SuppressWarnings("unused")
	private void setId(Integer id) {
		this.id = id;
	}

	public SOStatusType getSo_status_type_fk() {
		return so_status_type_fk;
	}

	public void setSo_status_type_fk(SOStatusType so_status_type_fk) {
		this.so_status_type_fk = so_status_type_fk;
	}

	public ServiceRequest getService_request_fk() {
		return service_request_fk;
	}

	public void setService_request_fk(ServiceRequest service_request_fk) {
		this.service_request_fk = service_request_fk;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Set<ServiceDevice> getDevices() {
		return devices;
	}

	public void setDevices(Set<ServiceDevice> devices) {
		this.devices = devices;
	}

	public Set<ServiceAction> getActions() {
		return actions;
	}

	public void setActions(Set<ServiceAction> actions) {
		this.actions = actions;
	}

	public Set<ServicePart> getParts() {
		return parts;
	}

	public void setParts(Set<ServicePart> parts) {
		this.parts = parts;
	}

	public Set<Service> getServices() {
		Set<Service> services = new HashSet<>();
		services.addAll(getActions());
		services.addAll(getParts());
		return services;
	}

	public void setServices(Set<Service> services) {
		Set<ServiceAction> newActions = new HashSet<>();
		Set<ServicePart> newParts = new HashSet<>();
		for (Service service : services) {
			if (service instanceof ServiceAction) {
				newActions.add((ServiceAction) service);
			} else {
				newParts.add((ServicePart) service);
			}
		}
		setActions(newActions);
		setParts(newParts);
	}

}
