package db.model;

import java.util.Date;

public class UserAccount {

	private Integer user_account;
	private SubjectType subject_type_fk;
	private Integer subject_fk;
	private String username;
	private String passw;
	private Integer status;
	private Date valid_from;
	private Date valid_to;
	private Employee created_by;
	private Date created;
	private Character password_never_expires;

	public Integer getUser_account() {
		return user_account;
	}

	@SuppressWarnings("unused")
	private void setUser_account(int user_account) {
		this.user_account = user_account;
	}

	public SubjectType getSubject_type_fk() {
		return subject_type_fk;
	}

	public void setSubject_type_fk(SubjectType subject_type_fk) {
		this.subject_type_fk = subject_type_fk;
	}

	public int getSubject_fk() {
		return subject_fk;
	}

	public void setSubject_fk(int subject_fk) {
		this.subject_fk = subject_fk;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassw() {
		return passw;
	}

	public void setPassw(String passw) {
		this.passw = passw;
	}

	public int isStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getValid_from() {
		return valid_from;
	}

	public void setValid_from(Date valid_from) {
		this.valid_from = valid_from;
	}

	public Date getValid_to() {
		return valid_to;
	}

	public void setValid_to(Date valid_to) {
		this.valid_to = valid_to;
	}

	public Employee getCreated_by() {
		return created_by;
	}

	public void setCreated_by(Employee created_by) {
		this.created_by = created_by;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Character getPassword_never_expires() {
		return password_never_expires;
	}

	public void setPassword_never_expires(Character password_never_expires) {
		this.password_never_expires = password_never_expires;
	}
}
