package db.model;

public class ServiceRequestStatusType {

	public static final int registered = 1;
	public static final int rejected = 2;
	public static final int ordered = 3;

	private int id;
	private String type_name;

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType_name() {
		return type_name;
	}

	public void setType_name(String type_name) {
		this.type_name = type_name;
	}
}
