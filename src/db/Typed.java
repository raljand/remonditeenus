package db;

import java.util.Date;
import java.util.Set;

import app.libs.StdLib;
import db.dao.DaoLib;
import db.model.Device;
import db.model.Employee;
import db.model.SOStatusType;
import db.model.ServiceDevice;
import db.model.ServiceOrder;
import db.model.ServiceRequest;
import db.model.ServiceRequestStatusType;

public class Typed<T> {

	public final Class<T> type;
	public final T value;

	public Typed(Class<T> type, T value) {
		if (!(value == null || type.isInstance(value))) {
			throw new IllegalArgumentException("Value \""
					+ value.getClass().getName()
					+ "\" is not an instance of \"" + type.getName() + "\".");
		}
		this.type = type;
		this.value = value;
	}

	public static Typed<Date> date(Date date) {
		return new Typed<Date>(Date.class, date);
	}

	public static <T> Typed<T> dbLoad(Class<T> class_, int id) {
		return new Typed<T>(class_, DaoLib.load(class_, id));
	}

	public static Typed<Device> device(Device device) {
		return new Typed<Device>(Device.class, device);
	}

	public static Typed<Employee> employee(Employee employee) {
		return new Typed<Employee>(Employee.class, employee);
	}

	public static Typed<Integer> integer(Integer value) {
		return new Typed<Integer>(Integer.class, value);
	}

	public static Typed<Service> service(Service service) {
		return new Typed<Service>(Service.class, service);
	}

	public static Typed<ServiceOrder> serviceOrder(ServiceOrder order) {
		return new Typed<ServiceOrder>(ServiceOrder.class, order);
	}

	public static Typed<ServiceRequest> serviceRequest(ServiceRequest request) {
		return new Typed<ServiceRequest>(ServiceRequest.class, request);
	}

	public static Typed<ServiceRequestStatusType> statusType(
			ServiceRequestStatusType status) {
		return new Typed<ServiceRequestStatusType>(
				ServiceRequestStatusType.class, status);
	}

	public static Typed<String> string(String value) {
		return new Typed<>(String.class, value);
	}

	public static Typed<Set<Service>> serviceSet() {
		return empty(StdLib.<Service> classOfSet());
	}

	public static <T> Typed<Set<T>> set(Set<T> set) {
		return new Typed<>(StdLib.<T> classOfSet(), set);
	}

	public static <T> Typed<T> empty(Class<T> type) {
		return new Typed<>(type, null);
	}

	public static Typed<ServiceDevice> serviceDevice(ServiceDevice arg0) {
		return new Typed<ServiceDevice>(ServiceDevice.class, arg0);
	}

	public static Typed<SOStatusType> soStatusType(SOStatusType type) {
		return new Typed<SOStatusType>(SOStatusType.class, type);
	}

}
