package db.dao;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class DBConnectionManager {

	private static Connection connection;
	private static String url;

	public static Connection getConnection() {
		if (connection == null) {
			ResourceBundle bundle = ResourceBundle.getBundle("DBConnection");
			try {
				Class.forName(bundle.getString("driver"));
				url = bundle.getString("url");
				String username = bundle.getString("usr");
				String password = bundle.getString("pwd");
				connection = DriverManager.getConnection(url, username,
						password);
			} catch (ClassNotFoundException | SQLException e) {
				throw new RuntimeException(e);

			}
		}
		return connection;
	}

	public static void close() {
		if (url != null) {
			try {
				Driver driver = DriverManager.getDriver(url);
				DriverManager.deregisterDriver(driver);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
