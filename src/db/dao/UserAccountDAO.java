package db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.codec.digest.DigestUtils;

import db.model.UserAccount;

public class UserAccountDAO {

	public static Integer getId(String username, String password) {
		Connection connection = DBConnectionManager.getConnection();
		try {
			String hashed = DigestUtils.md5Hex(password);
			PreparedStatement statement = connection
					.prepareStatement("SELECT user_account"
							+ " FROM user_account WHERE username=?"
							+ " AND passw=?;");
			statement.setString(1, username);
			statement.setString(2, hashed);

			ResultSet result = statement.executeQuery();

			if (result.next()) {
				return result.getInt("user_account");
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public static <T> T subject(UserAccount account) {
		return SubjectDAO.load(account.getSubject_fk(),
				account.getSubject_type_fk());
	}
}
