package db.dao;

import db.model.SubjectType;

public class SubjectDAO {
	private SubjectDAO() {
	}

	public static <T> T getSubject(int id, int type) {
		return DaoLib.load(DaoLib.<T> subjectClass(type, id), id);
	}

	public static <T> T load(int id, SubjectType subjectType) {
		return getSubject(id, subjectType.getId());
	}
}
