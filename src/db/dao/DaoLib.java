package db.dao;

import java.util.List;

import org.hibernate.Session;

import db.Typed;
import db.model.Employee;
import db.model.Enterprise;
import db.model.Person;

public class DaoLib {

	private DaoLib() {
	}

	public static <T> T load(Class<T> aClass, Integer id) {
		return (T) DBSessionManager.getSession().load(aClass, id);
	}

	public static <T> void save(Typed<T> object) {
		if (object.value == null) {
			throw new IllegalArgumentException("Is not supposed to be null: "
					+ object.type.getName());
		}
		DBSessionManager.getSession().save(object.value);
	}

	public static <T> List<T> list(Class<T> aClass) {
		Session session = DBSessionManager.getSession();
		return session.createQuery("from " + aClass.getSimpleName()).list();
	}

	public static <T> Class<T> subjectClass(int type, int id) {
		switch (type) {
		case 1:
			return (Class<T>) Person.class;
		case 2:
			return (Class<T>) Enterprise.class;
		case 3:
			return (Class<T>) Employee.class;
		default:
			throw new IllegalArgumentException("No such type: "
					+ Integer.toString(type));
		}
	}
}
