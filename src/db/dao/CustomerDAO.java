package db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import db.model.Customer;

public class CustomerDAO {
	
	public static Set<Customer> findByName(String name) {
		Connection connection = DBConnectionManager.getConnection();
		try {
			String searchTerm = "%" + name + "%";
			
			PreparedStatement statement = connection
					.prepareStatement(getSQLSearchString());
			statement.setString(1, searchTerm);
			statement.setString(2, searchTerm);

			ResultSet result = statement.executeQuery();
			
			Set<Customer> customers = new HashSet<>();
			
			while(result.next()) {
				Customer customer = new Customer();
				customer.setId(result.getInt("customer"));
				customer.setSubjectTypeKey(result.getInt("subject_type_fk"));
				customer.setSubjectKey(result.getInt("subject_fk"));
				customers.add(customer);
			}
			
			return customers;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	private static String getSQLSearchString() {
		StringBuilder builder = new StringBuilder();
		builder.append("select customer, subject_type_fk, subject_fk");
		builder.append(" from customer where");
		builder.append(" (subject_type_fk = 1 and exists (");
		builder.append("select person from person");
		builder.append(" where subject_fk = person and last_name ilike ?");
		builder.append(")) or (subject_type_fk = 2 and exists (");
		builder.append("select enterprise from enterprise");
		builder.append(" where subject_fk = enterprise and name ilike ?");
		builder.append("))");
		return builder.toString();
	}
}
