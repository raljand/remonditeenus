package db;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import view.field.model.ModelValue;

public class ModelLib {

	public static Integer id(Typed<?> model) {
		return annotatedValue(model, Id.class);
	}

	public static String slug(Typed<?> model) {
		return annotatedValue(model, Slug.class);
	}

	private static <T> T annotatedValue(Typed<?> model,
			Class<? extends Annotation> annotation) {
		Method[] methods = model.type.getMethods();
		for (Method method : methods) {
			if (method.isAnnotationPresent(annotation)) {
				return invoke(method, model.value);
			}
		}

		throw new UnsupportedOperationException(model.type.getName() + " . "
				+ annotation.getName());
	}

	public static void setFields(Typed<?> model, List<ModelValue<?>> fields) {
		for (ModelValue<?> translation : fields) {
			invoke(model, translation.field, translation.translatedValue.value);
		}
	}

	public static <T> T invoke(Typed<?> model, ModelField<T> field, Object value) {
		if (model.value == null) {
			throw new IllegalArgumentException("Model \""
					+ model.type.getName() + "\" is not supposed to be empty.");
		}
		List<Class<?>> argTypes = new LinkedList<>();
		List<Object> argObjects = new LinkedList<>();
		String prefix;
		if (value != null) {
			argTypes.add(field.type);
			argObjects.add(value);
			prefix = "set";
		} else {
			prefix = "get";
		}
		return ModelLib.invoke(
				method(model.type, prefix + StringUtils.capitalize(field.name),
						argTypes.toArray(new Class<?>[argTypes.size()])),
				model.value, argObjects.toArray());
	}

	public static Method method(Class<?> owner, String name,
			Class<?>... paramTypes) {
		try {
			return owner.getMethod(name, paramTypes);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}

	public static <T> T invoke(Method method, Object instance, Object... args) {
		try {
			return (T) method.invoke(instance, args);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	public static <T> Typed<T> newTyped(Class<T> type) {
		return new Typed<>(type, newInstance(type));
	}

	public static <T> T newInstance(Class<T> type) {
		try {
			return type.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
}
