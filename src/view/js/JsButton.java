package view.js;

public class JsButton {
	public final String label;
	public final JsFunctionLink function;

	public JsButton(String label, JsFunctionLink function) {
		this.label = label;
		this.function = function;
	}
}
