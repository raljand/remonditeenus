package view.js;

import java.util.Arrays;
import java.util.List;

import view.field.Name;
import app.libs.ListLib;
import app.libs.StringLib;

public class JsFunction {

	public final String name;
	public final List<String> arguments;

	private JsFunction(String name, List<String> arguments) {
		this.name = name;
		this.arguments = arguments;
	}

	public static JsFunction searchCustomer() {
		return withThis("searchCustomer", Arrays.<String> asList());
	}

	public static JsFunction devicesNewPart(Name name) {
		return withNames("devices.newPart", name);
	}

	public static JsFunction devicesNewService(Name name) {
		return withNames("devices.newService", name);
	}

	private static JsFunction withNames(String funName, Name name) {
		return withThis(funName,
				Arrays.asList(StringLib.jsArray(ListLib.names(name))));
	}

	private static JsFunction withThis(String name, List<String> arguments) {
		return new JsFunction(name, ListLib.append(Arrays.asList("this"),
				arguments));
	}
}
