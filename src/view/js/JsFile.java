package view.js;

import java.util.Arrays;
import java.util.List;

public class JsFile {

	public final String path;
	public final List<JsFile> dependencies;

	private JsFile(String path, List<JsFile> dependencies) {
		this.path = path;
		this.dependencies = dependencies;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof JsFile) {
			return path.equals(((JsFile) obj).path);
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return path.hashCode();
	}

	public static JsFile customerSearch() {
		return new JsFile("customerSearch.js", Arrays.asList(JsFile.jQuery(),
				base()));
	}

	public static JsFile devices() {
		return new JsFile("devices.js", Arrays.asList(JsFile.jQuery(), base()));
	}

	private static JsFile base() {
		return new JsFile("base.js", Arrays.asList(JsFile.jQuery()));
	}

	private static JsFile jQuery() {
		return new JsFile(
				"//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.js",
				Arrays.<JsFile> asList());
	}
}
