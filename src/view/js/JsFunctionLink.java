package view.js;

public class JsFunctionLink {

	public final JsFunction function;
	public final JsFile location;

	public JsFunctionLink(JsFunction function, JsFile location) {
		this.function = function;
		this.location = location;
	}
}
