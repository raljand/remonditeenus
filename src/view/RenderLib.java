package view;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;

import view.field.model.ActionLink;
import view.field.model.FieldPair;
import view.field.rendering.Tag;
import view.js.JsButton;
import view.js.JsFile;
import view.js.JsFunction;
import app.Parameter;
import app.libs.ListLib;
import app.libs.StdLib;
import app.libs.StringLib;

import com.google.common.base.Function;

import db.ModelLib;
import db.Typed;

public class RenderLib {

	public static <T> Tag render(final Form<T> form) {
		return formWrap(ListLib.<Tag> flatten(Arrays.asList(form.model == null
				|| form.model.model.value == null ? Arrays.<Tag> asList()
				: ListLib.map(new Function<ActionLink<T>, Tag>() {
					@Override
					public Tag apply(ActionLink<T> arg0) {
						return arg0.action.isValid(form.model.model.value) ? Tag
								.tr(Arrays.asList(Tag.td(Tag.a(arg0.link))))
								: null;
					}
				}, form.model.actions), ListLib.map(
				new Function<FieldPair<?, ?>, Tag>() {
					@Override
					public Tag apply(FieldPair<?, ?> arg0) {
						return render(
								arg0,
								form.model == null ? Typed
										.empty(arg0.emptyValue.type)
										: form.model.model);
					}
				}, form.fields))), form.buttonLabel);
	}

	private static Tag formWrap(List<Tag> content, String buttonLabel) {
		return Tag.form(Tag.table(ListLib.flatten(Arrays.asList(content, Arrays
				.asList(Tag.tr(Arrays.asList(Tag.withParameter(Parameter
						.colspan(2),
						Tag.td(Tag.input(Arrays.asList(
								Parameter.type("submit"),
								Parameter.value(buttonLabel))))))))))));
	}

	static <Model, Render> Tag render(FieldPair<Model, Render> field,
			Typed<?> model) {
		return field.formField.renderer.render(
				field.formField.value != null ? field.formField.value
						: field.translator.renderable(field.modelField == null
								|| model.value == null ? field.emptyValue
								: new Typed<Model>(field.modelField.type,
										ModelLib.invoke(model,
												field.modelField, null))),
				field.formField.name);
	}

	public static List<Tag> render(List<JsButton> buttons) {
		return ListLib.map(new Function<JsButton, Tag>() {
			@Override
			public Tag apply(JsButton button) {
				return Tag.button(render(button.function.function),
						button.label);
			}
		}, buttons);
	}

	public static String render(JsFunction function) {
		return function.name + "("
				+ StringLib.join(null, function.arguments, ", ") + ")";
	}

	public static List<Tag> scriptTags(JsFile jsFile) {
		return StdLib.removeNull(ListLib.map(new Function<JsFile, Tag>() {
			@Override
			public Tag apply(JsFile arg0) {
				return Tag.script(arg0.path);
			}
		}, dependencies(jsFile)));
	}

	private static LinkedHashSet<JsFile> dependencies(JsFile file) {
		LinkedHashSet<JsFile> paths = new LinkedHashSet<>();
		for (JsFile dependency : file.dependencies) {
			paths.addAll(dependencies(dependency));
		}
		paths.add(file);
		return paths;
	}
}
