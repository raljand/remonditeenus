package view;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import view.field.Field;
import view.field.model.ActionLink;
import view.field.model.FieldPair;
import db.Typed;
import db.model.ServiceOrder;
import db.model.ServiceRequest;

public class Form<T> {

	public final List<FieldPair<?, ?>> fields;
	public final String buttonLabel;
	public final FormModel<T> model;

	private Form(List<FieldPair<?, ?>> fields, String buttonLabel,
			FormModel<T> model) {
		this.fields = fields;
		this.buttonLabel = buttonLabel;
		this.model = model;
	}

	public static Form<?> copy(Form<?> form, FormModel<?> model) {
		return new Form<>(form.fields, form.buttonLabel, model);
	}

	public static Form<?> copy(Form<?> form, List<FieldPair<?, ?>> fields) {
		return new Form<>(fields, form.buttonLabel, form.model);
	}

	public static Form<?> login() {
		return new Form<>(
				Arrays.<FieldPair<?, ?>> asList(FieldPair.modelless(
						Field.strInput("Kasutajanimi", "username", "text"),
						String.class), FieldPair.modelless(
						Field.strInput("Parool", "password", "password"),
						String.class)), "Logi sisse", null);
	}

	public static Form<ServiceRequest> serviceRequest(
			final HttpServletRequest req, ServiceRequest instance,
			String idParam) {
		final Typed<ServiceRequest> model = Typed.serviceRequest(instance);

		return new Form<>(Arrays.<FieldPair<?, ?>> asList(
				FieldPair.customer("customer", "customer"),
				FieldPair.customerDescription(),
				FieldPair.employeeDescription(), FieldPair.employee(),
				FieldPair.serviceRequestStatus(), FieldPair.created()),
				"Salvesta", new FormModel<>(model,
						Arrays.<ActionLink<ServiceRequest>> asList(
								ActionLink.requestRejection(req, idParam),
								ActionLink.orderRegistration(model))));
	}

	public static Form<ServiceOrder> orderForm(ServiceRequest request,
			ServiceOrder order) {
		return new Form<ServiceOrder>(Arrays.<FieldPair<?, ?>> asList(
				FieldPair.serviceRequest(request), FieldPair.soStatusType(),
				FieldPair.devicesSet("devicesname"), FieldPair.serviceSet(),
				FieldPair.orderNote("note")), "Salvesta",
				FormModel.serviceOrder(order));
	}
}
