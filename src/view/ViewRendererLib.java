package view;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import view.field.rendering.Tag;
import app.Function3;
import app.Parameter;
import app.libs.StringLib;

import com.google.common.base.Function;

import controller.RequestLib;
import db.ModelLib;
import db.Typed;
import db.dao.DaoLib;
import db.model.ServiceRequest;

public class ViewRendererLib {

	public static <T> Callable<String> model(
			Function3<HttpServletRequest, HttpServletResponse, T, Form<?>> formFunc,
			String idParam, Function<T, List<String>> renderFunc,
			HttpServletRequest req, HttpServletResponse resp, Typed<T> model,
			String caption) throws IOException {
		if (model.value == null) {
			return ViewRendererLib.list(DaoLib.list(model.type), renderFunc,
					idParam, model.type, caption);
		} else {
			Form<?> form = formFunc.apply(req, resp, model.value);
			RequestLib.tryAction(form.model.actions, req, idParam);
			return ViewRendererLib.form(form);
		}
	}

	public static Callable<String> form(final Form<?> form) {
		return new Callable<String>() {
			@Override
			public String call() throws Exception {
				return StringLib.tag(RenderLib.render(form));
			}
		};
	}

	public static Callable<String> form(final Tag prefix, final Form<?> form) {
		return new Callable<String>() {
			@Override
			public String call() throws Exception {
				return StringLib.tags(Arrays.asList(prefix,
						RenderLib.render(form)));
			}
		};
	}

	public static Callable<String> requests(List<ServiceRequest> requests,
			final String idParam) {
		return list(requests, new Function<ServiceRequest, List<String>>() {
			@Override
			public List<String> apply(ServiceRequest arg0) {
				return Arrays.<String> asList(arg0.getCustomer().getSubject()
						.getName(), arg0.getStatus().getType_name(), arg0
						.getCreated().toString());
			}
		}, idParam, ServiceRequest.class, "Service requests");
	}

	private static <T> Callable<String> list(final List<T> items,
			final Function<T, List<String>> itemParams, final String idParam,
			final Class<T> type, final String caption) {
		return new Callable<String>() {
			@Override
			public String call() throws Exception {
				return "<table><caption>" + caption + "</caption>"
						+ StringLib.join(new Function<T, String>() {
							@Override
							public String apply(T arg0) {
								String id = ModelLib
										.id(new Typed<>(type, arg0)).toString();
								return "<tr>"
										+ "<td><a href=\""
										+ StringLib.normUrl("", Arrays
												.asList(new Parameter(idParam,
														id)))
										+ "\">"
										+ id
										+ "</a></td>"
										+ StringLib.join(
												new Function<String, String>() {
													@Override
													public String apply(
															String arg0) {
														return "<td>" + arg0
																+ "</td>";
													}
												}, itemParams.apply(arg0))
										+ "</tr>\n";
							}
						}, items) + "</table>";
			}
		};
	}

	private ViewRendererLib() {
	}
}
