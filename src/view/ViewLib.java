package view;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import view.field.model.Translator;
import app.libs.StdLib;

import com.google.gson.Gson;

public class ViewLib {

	public static String render(View view) {
		return StdLib.call(view.renderer);
	}

	public static void writeJson(Object object, HttpServletResponse response)
			throws IOException {
		response.setContentType("application/json");
		response.getWriter().write(new Gson().toJson(object));
	}

	public static <Model, Render> Model saveable(
			Translator<Model, Render> translator, Object value) {
		return translator.saveable((Render) value);
	}

	private ViewLib() {
	}
}
