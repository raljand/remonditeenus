package view.field;

import javax.servlet.http.HttpServletRequest;

public interface Parser<T> {
	HasError<? extends T> parse(HttpServletRequest req, Name name, Object model);
}
