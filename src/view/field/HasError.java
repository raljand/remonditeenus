package view.field;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import app.NullLessSet;

import com.google.common.base.Function;

public class HasError<T> {

	public final T value;
	public final boolean hasError;

	public HasError(T value, boolean hasError) {
		this.value = value;
		this.hasError = hasError;
	}

	public static <T> HasError<T> clean(T value) {
		return new HasError<T>(value, false);
	}

	public static <T> HasError<? extends T> parse(HttpServletRequest req,
			Object model, Field<T> field) {
		return field.parser.parse(req, field.name, model);
	}

	public static <F, T> HasError<? extends List<T>> map(
			Function<F, ? extends HasError<? extends T>> fun,
			Iterable<? extends F> items) {
		return map(new LinkedList<T>(), fun, items);
	}

	public static <F, T> HasError<? extends Set<T>> setMap(
			Function<F, ? extends HasError<? extends T>> fun,
			Iterable<? extends F> items) {
		return map(new NullLessSet<T>(), fun, items);
	}

	public static <F, T, U extends Collection<T>> HasError<U> map(U collection,
			Function<F, ? extends HasError<? extends T>> fun,
			Iterable<? extends F> items) {
		boolean hasError = false;
		for (F item : items) {
			HasError<? extends T> resultItem = fun.apply(item);
			hasError = resultItem.hasError ? true : hasError;
			collection.add(resultItem.value);
		}
		return new HasError<U>(collection, hasError);
	}
}
