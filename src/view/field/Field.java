package view.field;

import java.util.Arrays;
import java.util.Set;

import view.field.rendering.ConverterLib;
import view.field.rendering.RenderData;
import view.field.rendering.Renderer;
import view.field.rendering.RendererLib;
import view.field.rendering.SetRendererLib;
import view.js.JsButton;
import view.js.JsFile;
import view.js.JsFunction;
import view.js.JsFunctionLink;
import db.Service;
import db.model.Customer;
import db.model.ServiceDevice;
import db.model.ServiceRequest;

public class Field<T> {

	public final Renderer<T> renderer;
	public final Parser<T> parser;
	public final Name name;
	public final T value;

	public Field(Renderer<T> renderer, Parser<T> parser, Name name, T value) {
		this.renderer = renderer;
		this.parser = parser;
		this.name = name;
		this.value = value;
	}

	private static class Single<T> extends Field<RenderData<T>> {
		private Single(Renderer<RenderData<T>> renderer,
				Parser<RenderData<T>> translator, String name) {
			super(renderer, translator, Name.plain(name), null);
		}
	}

	public static <T> Field<T> copy(Field<T> source, T value) {
		return new Field<T>(source.renderer, source.parser, source.name, value);
	}

	public static Single<String> strInput(String label, String name, String type) {
		return new Single<>(RendererLib.<String> input(label, type,
				ConverterLib.<String> str()), ParserLib.str(), name);
	}

	public static Single<Customer> customerSearch(String name) {
		JsFile file = JsFile.customerSearch();
		return new Single<Customer>(RendererLib.search(ConverterLib
				.<Customer> model(), "Klient", file, Arrays
				.asList(new JsButton("Vali", new JsFunctionLink(JsFunction
						.searchCustomer(), file)))),
				ParserLib.model(Customer.class), name);
	}

	public static <T> Single<T> constant(Parser<RenderData<T>> valueFun) {
		return new Single<T>(RendererLib.<T> empty(), valueFun, null);
	}

	public static Single<ServiceRequest> serviceRequest() {
		return new Single<ServiceRequest>(
				RendererLib.<ServiceRequest> hidden(ConverterLib
						.<ServiceRequest> modelId()),
				ParserLib.model(ServiceRequest.class), "request");
	}

	public static Single<String> textarea(String name, String label) {
		return new Single<String>(RendererLib.textarea(
				ConverterLib.<String> str(), label), ParserLib.str(), name);
	}

	public static Field<Set<RenderData<Service>>> serviceSet() {
		String actionParam = "action";
		return new Field<>(SetRendererLib.serviceSet(actionParam, "part"),
				ParserLib.setOfService(actionParam),
				new Name("services", Arrays.asList("type", "description",
						"amount", "price")), null);
	}

	public static Field<Set<RenderData<ServiceDevice>>> devices(String name) {
		return new Field<Set<RenderData<ServiceDevice>>>(
				SetRendererLib.devices(), ParserLib.devices(),
				Name.plain(name), null);
	}
}
