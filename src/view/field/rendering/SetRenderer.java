package view.field.rendering;

import java.util.Set;

public interface SetRenderer<T> extends Renderer<Set<RenderData<T>>> {
}
