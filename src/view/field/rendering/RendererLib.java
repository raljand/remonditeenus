package view.field.rendering;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;

import view.RenderLib;
import view.field.Name;
import view.js.JsButton;
import view.js.JsFile;
import app.Parameter;
import app.libs.ListLib;
import app.libs.StringLib;

import com.google.common.base.Function;

import db.Typed;

public class RendererLib {

	public static <T> SingleRenderer<T> empty() {
		return new SingleRenderer<T>() {
			@Override
			public Tag render(RenderData<T> value, Name name) {
				return null;
			}
		};
	}

	public static <T> SingleRenderer<T> search(
			final Converter<T, ImmutablePair<Integer, String>> converter,
			final String header, final JsFile jsFile,
			final List<JsButton> jsButtons) {
		return new SingleRenderer<T>() {
			@Override
			public Tag render(RenderData<T> data, Name name) {
				ImmutablePair<Integer, String> pValue = converter
						.apply(data.object);

				List<Tag> content = ListLib.flatten(Arrays.asList(Arrays
						.<Tag> asList(Tag.input(
								"hidden",
								name.name,
								ConverterLib.<Integer> str().apply(
										Typed.integer(pValue.left))), Tag
								.withParameters(Arrays.asList(Parameter
										.class_("js-finder-slug")), Tag
										.span(pValue.right))), RenderLib
						.render(jsButtons), Arrays.asList(Tag.withParameters(
						Arrays.asList(Parameter.class_("is-hidden js-finder")),
						Tag.span(Tag.tags(Tag.input(Arrays.asList(
								Parameter.type("search"),
								Parameter.placeholder("Otsi"),
								Parameter.onkeydown("ignoreEnter(event)"))),
								Tag.ul()))))));

				List<Tag> tds = Arrays.asList(Tag.withParameter(
						Parameter.class_("js-finder-parent"),
						Tag.td(StringLib.tags(content))));

				return Tag.js(
						header,
						jsFile,
						data.error == null ? tds : ListLib.append(tds,
								Arrays.asList(Tag.td(data.error))));
			}
		};
	}

	public static <T> SingleRenderer<T> textarea(
			final Function<Typed<T>, String> converter, final String label) {
		return new SingleRenderer<T>() {
			@Override
			public Tag render(RenderData<T> value, Name name) {
				return Tag.tr(
						label,
						Arrays.asList(Tag.td(Tag.textarea(name.name,
								converter.apply(value.object)))));
			}
		};
	}

	public static <T> SingleRenderer<T> hidden(
			final Converter<T, String> converter) {
		return new SingleRenderer<T>() {
			@Override
			public Tag render(RenderData<T> value, Name name) {
				return Tag.tr(Arrays.asList(Tag.td(Tag.input("hidden",
						name.name, converter.apply(value.object)))));
			}
		};
	}

	public static <T> SingleRenderer<T> input(String label, final String type,
			final Converter<T, String> converter) {
		return input(label, type, converter, Arrays.<Parameter> asList());
	}

	public static <T> SingleRenderer<T> input(String label, final String type,
			final Converter<T, String> converter, final String class_) {
		return input(label, type, converter, class_, false);
	}

	public static <T> SingleRenderer<T> input(String label, final String type,
			final Converter<T, String> converter, final String class_,
			final boolean disabled) {
		return input(label, type, converter, Arrays.asList(new Parameter(
				"class", class_), new Parameter("disabled", disabled ? ""
				: null)));
	}

	public static <T> SingleRenderer<T> input(final String label,
			final String type, final Converter<T, String> converter,
			final List<Parameter> params) {
		return new SingleRenderer<T>() {
			@Override
			public Tag render(RenderData<T> value, Name name) {
				return Tag.tr(
						label,
						Arrays.asList(Tag.td(Tag.withParameters(
								params,
								Tag.input(type, name.name,
										converter.apply(value.object))))));
			}
		};
	}

	private RendererLib() {
	}
}
