package view.field.rendering;

import javax.servlet.http.HttpServletRequest;

import app.libs.FunctionLib;

import com.google.common.base.Function;

import db.model.ServiceRequest;

public class Link {

	public final Function<Void, String> url;
	public final String label;

	public Link(String label, Function<Void, String> url) {
		this.label = label;
		this.url = url;
	}

	public static Link device() {
		return new Link("Registreeri uus seade",
		        FunctionLib.constant("/device"));
	}

	public static Link actionLink(String label, HttpServletRequest req,
	        String idParam, String name) {
		return new Link(label, FunctionLib.actionLink(req, idParam, name));
	}

	public static Link orderRegistration(ServiceRequest order) {
		return new Link("Registreeri tellimus",
		        FunctionLib.orderRegistration(order));
	}
}
