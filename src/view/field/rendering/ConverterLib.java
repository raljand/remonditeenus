package view.field.rendering;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.ImmutablePair;

import db.ModelLib;
import db.Typed;

public class ConverterLib {

	public static <T> Converter<T, String> constant(final Object value) {
		return new Converter<T, String>() {
			@Override
			public String apply(Typed<T> arg0) {
				return ConverterLib.str().apply(new Typed<>(null, value));
			}
		};
	}

	public static <T> Converter<T, String> str() {
		return new Converter<T, String>() {
			@Override
			@Nullable
			public String apply(@Nullable Typed<T> value) {
				if (value.value != null) {
					return value.value.toString();
				} else {
					return "";
				}
			}
		};
	}

	public static <T> Converter<T, ImmutablePair<Integer, String>> model() {
		return new Converter<T, ImmutablePair<Integer, String>>() {
			@Override
			@Nullable
			public ImmutablePair<Integer, String> apply(@Nullable Typed<T> value) {
				boolean isNull = value.value == null;
				return new ImmutablePair<Integer, String>(isNull ? null
						: ModelLib.id(value), isNull ? ""
						: ModelLib.slug(value));
			}
		};
	}

	public static <T> Converter<T, String> modelId() {
		return new Converter<T, String>() {
			@Override
			@Nullable
			public String apply(@Nullable Typed<T> value) {
				return value.value == null ? "" : ModelLib.id(value).toString();
			}
		};
	}
}
