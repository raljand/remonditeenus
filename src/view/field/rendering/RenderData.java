package view.field.rendering;

import db.Typed;

public final class RenderData<T> {

	public final Typed<T> object;
	public final String error;

	public RenderData(Typed<T> value, String error) {
		this.object = value;
		this.error = error;
	}

	public static <T> RenderData<T> clean(Typed<T> value) {
		return new RenderData<>(value, null);
	}

	public static <T> RenderData<T> error(Class<T> type, String error) {
		return new RenderData<>(Typed.<T> empty(type), error);
	}
}
