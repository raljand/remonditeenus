package view.field.rendering;

import java.util.Arrays;
import java.util.List;

import view.RenderLib;
import view.js.JsFile;
import app.Parameter;
import app.libs.ListLib;
import app.libs.StringLib;

import com.google.common.base.Function;

public class Tag {
	public final String name;
	public final List<Parameter> params;
	public final String content;

	private Tag(String name, List<Parameter> params, String content) {
		this.name = name;
		this.params = params;
		this.content = content;
	}

	public static Tag withParameter(Parameter param, Tag tag) {
		return withParameters(Arrays.asList(param), tag);
	}

	public static Tag withParameters(List<Parameter> params, Tag tag) {
		return new Tag(tag.name, ListLib.<Parameter> flatten(Arrays.asList(
				tag.params, params)), tag.content);
	}

	public static Tag button(String function, String content) {
		return new Tag("button", Arrays.<Parameter> asList(
				Parameter.type("button"),
				Parameter.onclick("javascript:" + function + ";")), content);
	}

	public static Tag form(Tag table) {
		return new Tag("form", Arrays.asList(Parameter.method("post")),
				tags(table));
	}

	public static Tag h1(String content) {
		return blandTag("h1", content);
	}

	public static Tag p(String content) {
		return blandTag("p", content);
	}

	public static Tag select(List<Parameter> params, List<Tag> options) {
		return container("select", params, options);
	}

	public static Tag container(String name, List<Parameter> params,
			List<Tag> tags) {
		return new Tag(name, params, Tag.tags(tags));
	}

	public static Tag table(List<Tag> trs) {
		return blandTag("table", tags(trs));
	}

	public static <T> Tag js(final String header, final JsFile jsFile,
			final List<Tag> tags) {
		return tr(header, ListLib.append(tags, RenderLib.scriptTags(jsFile)));
	}

	public static <T> Tag js(final String header, final JsFile jsFile,
			final Tag tag) {
		return tr(
				header,
				ListLib.append(Arrays.asList(Tag.td(tag)),
						RenderLib.scriptTags(jsFile)));
	}

	public static Tag th(String label) {
		return blandTag("th", label);
	}

	public static Tag tr(String label, List<Tag> tds) {
		return tr(ListLib.append(Arrays.asList(th(label)), tds));
	}

	public static Tag tr(List<Tag> tds) {
		return blandTag("tr", tags(tds));
	}

	public static Tag td(Tag content) {
		return td(tags(Arrays.asList(content)));
	}

	public static Tag td(String content) {
		return blandTag("td", content);
	}

	public static Tag script(String path) {
		return Tag.withParameters(Arrays.asList(new Parameter("src", StringLib
				.isUrlRelative(path) ? StringLib.normalisedUrl("/static/js/"
				+ path) : path)), baseScript(""));
	}

	private static Tag baseScript(String content) {
		return new Tag("script", Arrays.asList(new Parameter("type",
				"text/javascript"), Parameter.defer()), content);
	}

	public static Tag a(Link link) {
		return new Tag("a", Arrays.asList(new Parameter("href", StringLib
				.normalisedUrl(link.url.apply(null)))), link.label);
	}

	public static Tag div(String content) {
		return blandTag("div", content);
	}

	public static Tag span(String content) {
		return blandTag("span", content);
	}

	public static Tag textarea(String name, String content) {
		return new Tag("textarea", Arrays.asList(new Parameter("name", name),
				new Parameter("required", "")), content);
	}

	public static Tag ul() {
		return blandTag("ul", "");
	}

	public static Tag input(String type, String name, String value) {
		return withParameters(Arrays.asList(Parameter.value(value)),
				minInput(type, name));
	}

	public static Tag minInput(String type, String name) {
		return input(Arrays.<Parameter> asList(new Parameter("type", type),
				new Parameter("name", name), new Parameter("required", "")));
	}

	public static Tag input(List<Parameter> params) {
		return new Tag("input", params, null);
	}

	public static Tag option(String content) {
		return option(Arrays.<Parameter> asList(), content);
	}

	public static Tag option(String value, String repr) {
		return option(Arrays.asList(Parameter.value(value)), repr);
	}

	public static Tag option(List<Parameter> params, String string) {
		return new Tag("option", params, string);
	}

	private static Tag blandTag(String name, String content) {
		return new Tag(name, Arrays.<Parameter> asList(), content);
	}

	public static String tags(Tag... tags) {
		return StringLib.join(new Function<Tag, String>() {
			@Override
			public String apply(Tag arg0) {
				return arg0 == null ? null : StringLib.tag(arg0);
			}
		}, Arrays.asList(tags));
	}

	@Deprecated
	public static String tags(final List<Tag> tags) {
		return StringLib.join(new Function<Tag, String>() {
			@Override
			public String apply(Tag arg0) {
				return arg0 == null ? null : StringLib.tag(arg0);
			}
		}, tags);
	}

}
