package view.field.rendering;

import java.util.Arrays;

import app.Parameter;

public class InputLib {

	public static Tag input(String type, String name, String value) {
		return withValue(value, withName(name, input(type)));
	}

	public static Tag withValue(String value, Tag input) {
		return Tag.withParameter(Parameter.value(value), input);
	}

	public static Tag withName(String name, Tag input) {
		return Tag.withParameter(Parameter.name(name), input);
	}

	public static Tag input(String type) {
		return Tag.input(Arrays.<Parameter> asList(new Parameter("type", type),
				new Parameter("required", "")));
	}

	private InputLib() {
	}
}
