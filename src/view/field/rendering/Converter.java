package view.field.rendering;

import com.google.common.base.Function;

import db.Typed;

public interface Converter<F, T> extends Function<Typed<F>, T> {
}
