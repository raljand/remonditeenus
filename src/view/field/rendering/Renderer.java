package view.field.rendering;

import view.field.Name;

public interface Renderer<T> {
	Tag render(T value, Name name);
}
