package view.field.rendering;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import view.RenderLib;
import view.field.Name;
import view.js.JsButton;
import view.js.JsFile;
import view.js.JsFunction;
import view.js.JsFunctionLink;
import app.Parameter;
import app.libs.ListLib;
import app.libs.SetLib;
import app.libs.StringLib;

import com.google.common.base.Function;

import db.Service;
import db.dao.DaoLib;
import db.model.Device;
import db.model.ServiceDevice;
import db.model.ServicePart;

public class SetRendererLib {

	private SetRendererLib() {
	}

	public static SetRenderer<ServiceDevice> devices() {
		return new SetRenderer<ServiceDevice>() {
			@Override
			public Tag render(final Set<RenderData<ServiceDevice>> value,
					Name name) {
				final Set<Device> devices = SetLib.map(
						new Function<RenderData<ServiceDevice>, Device>() {
							@Override
							public Device apply(RenderData<ServiceDevice> arg0) {
								return arg0.object.value.getDevice_fk();
							}
						}, value);
				return Tag.tr("Seadmed", Arrays.asList(Tag.td(Tag.tags(Tag
						.select(Arrays.asList(Parameter.name(name.name),
								Parameter.multiple()), ListLib.map(
								new Function<Device, Tag>() {
									@Override
									public Tag apply(Device arg0) {
										List<Parameter> params = Arrays.asList(Parameter
												.value(Integer.toString(arg0
														.getId())));
										return Tag.option(
												devices.contains(arg0) ? ListLib.append(
														params,
														Arrays.asList(Parameter
																.selected()))
														: params, arg0
														.getName());
									}
								}, DaoLib.list(Device.class)))))));
			}
		};
	}

	public static SetRenderer<Service> serviceSet(final String actionParam,
			final String partParam) {
		return new SetRenderer<Service>() {
			@Override
			public Tag render(Set<RenderData<Service>> value, final Name name) {
				return set(new Function<RenderData<Service>, Tag>() {
					@Override
					public Tag apply(RenderData<Service> data) {
						Service service = data.object.value;
						Integer amount = service.getAmount();
						Integer price = service.getPrice();
						boolean isPart = service instanceof ServicePart;
						Iterator<String> names = name.iterator();
						List<Tag> tds = Arrays.asList(
								servicesTd1(names, isPart, actionParam,
										partParam, service),
								td2(isPart, names, amount), td3(names, price),
								td4(names, price, amount));
						return Tag.withParameter(Parameter
								.class_("js-devices-row"), Tag.tr(
								isPart ? "osa:" : "töö:",
								data.error == null ? tds : ListLib.append(tds,
										Arrays.asList(Tag.td(data.error)))));
					}
				}, value, name);
			}
		};
	}

	private static Tag servicesTd1(Iterator<String> name, boolean isPart,
			String actionParam, String partParam, Service service) {
		return Tag.td(StringLib.tags(Arrays.asList(Tag.span(StringLib
				.tags(Arrays.<Tag> asList(Tag.input("hidden", name.next(),
						isPart ? partParam : actionParam), Tag.input("text",
						name.next(), service.getDescription()), Tag
						.span(isPart ? "" : "teenus:<select>"
								+ "<option>naide</option>" + "</select>")))))));
	}

	private static Tag td2(boolean isPart, Iterator<String> name, Integer amount) {
		return Tag.td(StringLib.tags(Arrays.asList(Tag.span("kogus:"), Tag
				.withParameters(Arrays.asList(Parameter
						.class_("number-input js-devices-amount")), Tag.input(
						"number", name.next(), StringLib.safeStr(amount))), Tag
				.span("[" + (isPart ? "tk." : "tundi") + "]"))));
	}

	private static Tag td3(Iterator<String> name, Integer price) {
		return Tag.td(StringLib.tags(Arrays.asList(Tag.span("ühiku hind:"), Tag
				.withParameters(Arrays.asList(new Parameter("class",
						"number-input js-devices-price")), Tag.input("number",
						name.next(), StringLib.safeStr(price))))));
	}

	private static Tag td4(Iterator<String> name, Integer price, Integer amount) {
		return Tag.td(StringLib.tags(Arrays.asList(Tag.span("hind kokku:"), Tag
				.withParameters(Arrays.asList(
						Parameter.class_("number-input js-devices-sum"),
						new Parameter("disabled", "")), InputLib.withValue(
						StringLib.safeProduct(price, amount),
						InputLib.input("number"))))));
	}

	private static <T> Tag set(final Function<RenderData<T>, Tag> itemRenderer,
			Set<RenderData<T>> setValue, Name name) {
		JsFile jsFile = JsFile.devices();
		Tag bottomDiv = Tag.div(StringLib.tags(Arrays.asList(Tag.span(StringLib
				.tags(RenderLib.render(Arrays.asList(
						new JsButton("Uus varuosa", new JsFunctionLink(
								JsFunction.devicesNewPart(name), jsFile)),
						new JsButton("Uus töö", new JsFunctionLink(JsFunction
								.devicesNewService(name), jsFile)))))))));
		return Tag.js("Teenused", jsFile, Tag.withParameters(Arrays
				.asList(new Parameter("class", "js-data")), Tag.span(Tag.tags(
				Tag.withParameters(
						Arrays.asList(Parameter.class_("js-devices-table")),
						Tag.table(ListLib.map(itemRenderer, setValue))),
				bottomDiv))));
	}

}
