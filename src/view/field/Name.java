package view.field;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Name implements Iterable<String> {

	public final String name;
	public final List<String> subNames;

	public Name(String name, List<String> subNames) {
		this.name = name;
		this.subNames = subNames;
	}

	@Override
	public Iterator<String> iterator() {
		final Iterator<String> subNames = this.subNames.iterator();
		return new Iterator<String>() {
			@Override
			public void remove() {
				subNames.remove();
			}

			@Override
			public String next() {
				return Name.this.name + "_" + subNames.next();
			}

			@Override
			public boolean hasNext() {
				return subNames.hasNext();
			}
		};
	}

	public static Name plain(String name) {
		return new Name(name, Arrays.<String> asList());
	}
}
