package view.field;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import view.field.rendering.RenderData;
import app.Function3;
import app.libs.IterableLib;

import com.google.common.base.Function;

import controller.RequestLib;
import db.Service;
import db.Typed;
import db.dao.DaoLib;
import db.model.Device;
import db.model.Employee;
import db.model.ServiceAction;
import db.model.ServiceDevice;
import db.model.ServiceOrder;
import db.model.ServicePart;

public class ParserLib {

	private static final Logger logger = Logger.getLogger(ParserLib.class);

	public static <T> Parser<RenderData<T>> constant(final Typed<T> request) {
		return new Parser<RenderData<T>>() {
			@Override
			public HasError<RenderData<T>> parse(HttpServletRequest req,
					Name name, Object model) {
				return HasError.clean(RenderData.clean(request));
			}
		};
	}

	public static Parser<RenderData<Employee>> employee() {
		return new Parser<RenderData<Employee>>() {
			@Override
			public HasError<RenderData<Employee>> parse(HttpServletRequest req,
					Name name, Object model) {
				return HasError.clean(RenderData.clean(Typed
						.employee(RequestLib.<Employee> getSubject(req
								.getSession()))));
			}
		};
	}

	public static Parser<RenderData<Date>> now() {
		return new Parser<RenderData<Date>>() {
			@Override
			public HasError<RenderData<Date>> parse(HttpServletRequest req,
					Name name, Object model) {
				return new HasError<RenderData<Date>>(RenderData.clean(Typed
						.date(new Date())), false);
			}
		};
	}

	public static Parser<RenderData<String>> str() {
		return new Parser<RenderData<String>>() {
			@Override
			public HasError<RenderData<String>> parse(HttpServletRequest req,
					Name name, Object model) {
				return new HasError<RenderData<String>>(RenderData.clean(Typed
						.string(req.getParameter(name.name))), false);
			}
		};
	}

	public static <T> Parser<RenderData<T>> model(final Class<T> model) {
		return new Parser<RenderData<T>>() {
			@Override
			@Nullable
			public HasError<RenderData<T>> parse(
					@Nullable HttpServletRequest req, Name name,
					Object ownerModel) {
				String value = req.getParameter(name.name);
				try {
					return new HasError<RenderData<T>>(
							RenderData.clean(new Typed<T>(model, DaoLib
									.<T> load(model, Integer.valueOf(value)))),
							false);
				} catch (NumberFormatException e) {
					return new HasError<RenderData<T>>(RenderData.<T> error(
							model, "Klient peab olema valitud"), true);
				}
			}
		};
	}

	public static Parser<Set<RenderData<Service>>> setOfService(
			final String actionParam) {
		return set(new Function3<Name, Map<String, String>, Object, RenderData<Service>>() {
			@Override
			public RenderData<Service> apply(Name name,
					Map<String, String> values, Object model) {
				Iterator<String> names = name.iterator();
				boolean isAction = values.get(names.next()).equals(actionParam);
				Service service = isAction ? new ServiceAction()
						: new ServicePart();
				try {
					service.setService_order_fk((ServiceOrder) model);
					service.setDescription(values.get(names.next()));
					service.setAmount(Integer.valueOf(values.get(names.next())));
					service.setPrice(Integer.valueOf(values.get(names.next())));
					return RenderData.clean(Typed.service(service));
				} catch (NumberFormatException e) {
					return new RenderData<>(Typed.service(service),
							"Kogus ja hind peavad olema arvud.");
				}
			}
		});
	}

	public static Parser<Set<RenderData<ServiceDevice>>> devices() {
		return set(new Function3<Name, Map<String, String>, Object, RenderData<ServiceDevice>>() {
			@Override
			public RenderData<ServiceDevice> apply(Name arg0,
					Map<String, String> arg1, Object arg2) {
				Device device = DaoLib.load(Device.class,
						Integer.valueOf(arg1.get(arg0.name)));
				logger.debug("ParserLib.devices.set: " + device);
				ServiceDevice service = new ServiceDevice();
				service.setDevice_fk(device);
				service.setService_order_fk((ServiceOrder) arg2);
				return new RenderData<>(Typed.serviceDevice(service),
						device == null ? "Device peab eksisteerima" : null);
			}
		});
	}

	public static <T> Parser<Set<RenderData<T>>> set(
			final Function3<Name, Map<String, String>, Object, RenderData<T>> linker) {
		return new Parser<Set<RenderData<T>>>() {
			@Override
			@Nullable
			public HasError<? extends Set<RenderData<T>>> parse(
					@Nullable HttpServletRequest req, final Name name,
					final Object model) {
				return HasError
						.<Map<String, String>, RenderData<T>> setMap(
								new Function<Map<String, String>, HasError<RenderData<T>>>() {
									@Override
									public HasError<RenderData<T>> apply(
											Map<String, String> arg0) {
										RenderData<T> item = linker.apply(name,
												arg0, model);
										return new HasError<RenderData<T>>(
												item, item.error != null);
									}
								}, IterableLib.paramValues(req, name));
			}
		};
	}

	private ParserLib() {
	}

}
