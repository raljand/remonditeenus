package view.field.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import view.field.Field;
import view.field.HasError;
import view.field.Name;
import view.field.Parser;
import view.field.ParserLib;
import view.field.rendering.RenderData;
import app.NullLessSet;
import db.ModelField;
import db.Service;
import db.Typed;
import db.dao.DaoLib;
import db.model.Customer;
import db.model.Employee;
import db.model.SOStatusType;
import db.model.ServiceDevice;
import db.model.ServiceRequest;
import db.model.ServiceRequestStatusType;

public final class FieldPair<Model, Render> {

	public final Field<Render> formField;
	public final ModelField<Model> modelField;
	public final Translator<Model, Render> translator;
	public final Typed<Model> emptyValue;

	public FieldPair(Field<Render> formField, ModelField<Model> modelField,
			Translator<Model, Render> translator, Typed<Model> emptyValue) {
		this.formField = formField;
		this.modelField = modelField;
		this.translator = translator;
		this.emptyValue = emptyValue;
	}

	public static <Model, Render> FieldPair<Model, Render> copy(
			FieldPair<Model, Render> pair, Object value) {
		return new FieldPair<Model, Render>(Field.copy(pair.formField,
				(Render) value), pair.modelField, pair.translator,
				pair.emptyValue);
	}

	public static FieldPair<Customer, ?> customer(String fieldName, String name) {
		return single(Field.customerSearch(name), new ModelField<>(
				Customer.class, fieldName), Customer.class);
	}

	public static FieldPair<Employee, ?> employee() {
		return single(Field.constant(ParserLib.employee()),
				ModelField.employee(), Employee.class);
	}

	public static FieldPair<?, ?> orderNote(String string) {
		return single(Field.textarea(string, "Note"), ModelField.string("note"));
	}

	public static FieldPair<?, ?> serviceRequest(ServiceRequest request) {
		return single(Field.constant(ParserLib.constant(Typed
				.serviceRequest(request))), ModelField.serviceRequest(),
				ServiceRequest.class);
	}

	public static FieldPair<?, ?> soStatusType() {
		return single(Field.constant(ParserLib.constant(Typed.dbLoad(
				SOStatusType.class, 1))), ModelField.soStatusType(),
				SOStatusType.class);
	}

	public static FieldPair<?, ?> customerDescription() {
		return textarea("customerDescription", "Kliendi kirjeldus",
				"serviceDescByCustomer");
	}

	public static FieldPair<?, ?> employeeDescription() {
		return textarea("employeeDescription", "Töötaja kirjeldus",
				"serviceDescByEmployee");
	}

	public static FieldPair<String, ?> textarea(String fieldName, String label,
			String modelName) {
		return single(Field.textarea(fieldName, label),
				ModelField.string(modelName), String.class);
	}

	public static FieldPair<ServiceRequestStatusType, ?> serviceRequestStatus() {
		return single(
				Field.constant(new Parser<RenderData<ServiceRequestStatusType>>() {
					@Override
					public HasError<RenderData<ServiceRequestStatusType>> parse(
							HttpServletRequest req, Name name, Object model) {
						return HasError.clean(RenderData.clean(Typed
								.statusType(DaoLib.load(
										ServiceRequestStatusType.class,
										ServiceRequestStatusType.registered))));
					}
				}), new ModelField<>(ServiceRequestStatusType.class, "status"),
				ServiceRequestStatusType.class);
	}

	public static FieldPair<Date, ?> created() {
		return single(Field.constant(ParserLib.now()), new ModelField<>(
				Date.class, "created"), Date.class);
	}

	public static <T> FieldPair<T, ?> modelless(Field<RenderData<T>> formField,
			Class<T> type) {
		return new FieldPair<>(formField, null,
				TranslatorLib.<T> singleValued(), Typed.<T> empty(type));
	}

	@Deprecated
	private static <T> FieldPair<T, RenderData<T>> single(
			Field<RenderData<T>> formField, ModelField<T> modelField,
			Class<T> type) {
		return new FieldPair<T, RenderData<T>>(formField, modelField,
				TranslatorLib.<T> singleValued(),
				Typed.<T> empty(modelField.type));
	}

	private static <T> FieldPair<T, RenderData<T>> single(
			Field<RenderData<T>> formField, ModelField<T> modelField) {
		return new FieldPair<T, RenderData<T>>(formField, modelField,
				TranslatorLib.<T> singleValued(),
				Typed.<T> empty(modelField.type));
	}

	public static FieldPair<Set<Service>, ?> serviceSet() {
		return set(Field.serviceSet(), ModelField.serviceSet(), Service.class);
	}

	public static FieldPair<Set<ServiceDevice>, ?> devicesSet(String name) {
		return new FieldPair<>(Field.devices(name), ModelField.devices(),
				TranslatorLib.creatingSet(ServiceDevice.class),
				Typed.set(new HashSet<ServiceDevice>()));
	}

	private static <T> FieldPair<Set<T>, ?> set(
			Field<Set<RenderData<T>>> formField, ModelField<Set<T>> modelField,
			Class<T> type) {
		return new FieldPair<>(formField, modelField,
				TranslatorLib.<T> creatingSet(type),
				Typed.set(new NullLessSet<T>()));
	}

}
