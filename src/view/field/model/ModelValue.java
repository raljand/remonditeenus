package view.field.model;

import view.ViewLib;
import db.ModelField;
import db.Typed;

public class ModelValue<T> {

	public final ModelField<T> field;
	public final Typed<T> translatedValue;

	public ModelValue(ModelField<T> field, Typed<T> translatedValue) {
		this.field = field;
		this.translatedValue = translatedValue;
	}

	public static <Model, Render> ModelValue<?> saveable(
			FieldPair<Model, Render> pair, Object value) {
		return new ModelValue<>(pair.modelField, new Typed<>(
				pair.modelField.type, ViewLib.saveable(pair.translator, value)));
	}
}
