package view.field.model;

import java.util.Set;

import org.apache.log4j.Logger;

import view.field.rendering.RenderData;
import app.libs.SetLib;

import com.google.common.base.Function;

import db.Typed;
import db.dao.DaoLib;

public class TranslatorLib {

	private static Logger logger = Logger.getLogger(TranslatorLib.class);

	public static <T> Translator<T, RenderData<T>> constant(
			final Typed<T> outValue) {
		return new Translator<T, RenderData<T>>() {
			@Override
			public RenderData<T> renderable(Typed<T> value) {
				return RenderData.clean(outValue);
			}

			@Override
			public T saveable(RenderData<T> value) {
				return value.object.value;
			}
		};
	}

	public static <T> Translator<T, RenderData<T>> singleValued() {
		return new Translator<T, RenderData<T>>() {
			@Override
			public RenderData<T> renderable(Typed<T> value) {
				return RenderData.clean(value);
			}

			@Override
			public T saveable(RenderData<T> value) {
				return value.object.value;
			}
		};
	}

	public static <T> Translator<Set<T>, Set<RenderData<T>>> creatingSet(
			final Class<T> type) {
		return new Translator<Set<T>, Set<RenderData<T>>>() {
			@Override
			public Set<RenderData<T>> renderable(Typed<Set<T>> value) {
				return SetLib.map(new Function<T, RenderData<T>>() {
					@Override
					public RenderData<T> apply(T arg0) {
						return RenderData.clean(new Typed<>(type, arg0));
					}
				}, value.value);
			}

			@Override
			public Set<T> saveable(Set<RenderData<T>> value) {
				return SetLib.map(new Function<RenderData<T>, T>() {
					@Override
					public T apply(RenderData<T> arg0) {
						Typed<T> object = arg0.object;
						DaoLib.save(object);
						logger.debug("saveable: " + object.value);
						return object.value;
					}
				}, value);
			}
		};
	}

	private TranslatorLib() {
	}
}
