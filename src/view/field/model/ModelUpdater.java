package view.field.model;

import java.io.IOException;

public interface ModelUpdater<T> {
	
	boolean isValid(T model);

	void update(T model) throws IOException;
}
