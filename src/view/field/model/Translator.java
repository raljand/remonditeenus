package view.field.model;

import db.Typed;

public interface Translator<Model, Render> {

	Render renderable(Typed<Model> value);

	Model saveable(Render value);
}
