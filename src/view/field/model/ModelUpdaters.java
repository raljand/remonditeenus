package view.field.model;

import java.io.IOException;

public class ModelUpdaters {
	private ModelUpdaters() {
	}

	public static <T> ModelUpdater<T> empty() {
		return new ModelUpdater<T>() {
			@Override
			public boolean isValid(Object model) {
				return true;
			}

			@Override
			public void update(Object model) throws IOException {
			}
		};
	}

}
