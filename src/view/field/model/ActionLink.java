package view.field.model;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import view.field.rendering.Link;
import controller.RequestLib;
import db.Typed;
import db.dao.DaoLib;
import db.model.ServiceRequest;
import db.model.ServiceRequestStatusType;

public class ActionLink<T> {

	public static final String KEYWORD = "action";

	public final Link link;
	public final ModelUpdater<T> action;
	public final String name;
	public final Class<T> model;

	public ActionLink(Link link, ModelUpdater<T> action, Integer id,
	        String name, Class<T> model) {
		this.link = link;
		this.action = action;
		this.name = name;
		this.model = model;
	}

	public static ActionLink<ServiceRequest> requestRejection(
	        HttpServletRequest req, String idParam) {
		String actionName = "reject";
		return new ActionLink<>(Link.actionLink("Lükka tagasi",
		        req,
		        idParam,
		        actionName), new ModelUpdater<ServiceRequest>() {
			@Override
			public boolean isValid(ServiceRequest model) {
				ServiceRequestStatusType status = model.getStatus();
				return status.getId() == ServiceRequestStatusType.registered;
			}

			@Override
			public void update(ServiceRequest model) throws IOException {
				ServiceRequestStatusType status = DaoLib
				        .load(ServiceRequestStatusType.class,
				                ServiceRequestStatusType.rejected);
				model.setStatus(status);
			}
		}, RequestLib.number(req, idParam), actionName, ServiceRequest.class);
	}

	public static ActionLink<ServiceRequest> orderRegistration(
	        final Typed<ServiceRequest> model) {
		return empty(Link.orderRegistration(model.value));
	}

	public static <T> ActionLink<T> empty(Link link) {
		return new ActionLink<>(link, ModelUpdaters.<T> empty(), null, null,
		        null);
	}
}
