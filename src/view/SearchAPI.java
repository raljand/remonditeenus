package view;

public class SearchAPI {

	public final Integer key;
	public final String name;

	public SearchAPI(Integer key, String name) {
		this.key = key;
		this.name = name;
	}
}
