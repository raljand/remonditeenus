package view;

import java.util.Arrays;
import java.util.concurrent.Callable;

import view.field.rendering.Tag;
import app.libs.StringLib;

public class View {

	public final Callable<String> renderer;

	public View(Callable<String> renderer) {
		this.renderer = renderer;
	}

	public static View form(Form<?> form) {
		return new View(ViewRendererLib.form(form));
	}

	public static View error() {
		return new View(new Callable<String>() {
			@Override
			public String call() throws Exception {
				return StringLib.tags(Arrays.asList(
						Tag.h1("500 - Internal server error"),
						Tag.p("The server has crashed. It is time to panic!")));
			}
		});
	}
}
