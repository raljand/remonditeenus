package view;

import java.util.Arrays;
import java.util.List;

import view.field.model.ActionLink;
import db.Typed;
import db.model.ServiceOrder;

public final class FormModel<T> {

	public final Typed<T> model;
	public final List<ActionLink<T>> actions;

	public FormModel(Typed<T> model, List<ActionLink<T>> actions) {
		this.model = model;
		this.actions = actions;
	}

	public static <T> FormModel<T> copy(FormModel<T> source, Typed<T> newModel) {
		return new FormModel<>(newModel, source.actions);
	}

	public static FormModel<ServiceOrder> serviceOrder(ServiceOrder order) {
		return new FormModel<>(Typed.serviceOrder(order),
				Arrays.<ActionLink<ServiceOrder>> asList());
	}
}
