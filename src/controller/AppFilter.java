package controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.Transaction;

import view.View;
import app.Settings;
import app.libs.StringLib;
import db.dao.DBSessionManager;
import db.dao.DaoLib;
import db.model.UserAccount;

@WebFilter("/*")
public class AppFilter implements Filter {

	private Logger logger;

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		this.logger = Logger.getLogger(AppFilter.class);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResp = (HttpServletResponse) response;
		try {
			Transaction transaction = DBSessionManager.getSession()
					.beginTransaction();

			if (isIgnored(httpRequest.getRequestURI())
					|| hasSessionUser(httpRequest)) {
				chain.doFilter(request, response);
			} else {
				httpResp.sendRedirect(StringLib.normalisedUrl("/login"));
			}
			transaction.commit();
		} catch (Exception e) {
			if (Settings.DEBUG) {
				throw e;
			} else {
				logger.error("Hit an exception", e);
				ResponseLib.forward(View.error(), httpRequest, httpResp);
			}
		}
	}

	@Override
	public void destroy() {
	}

	private static boolean hasSessionUser(HttpServletRequest request) {
		try {
			return DaoLib.load(UserAccount.class, (int) request.getSession()
					.getAttribute("user")) != null;
		} catch (NullPointerException | ClassCastException e) {
			return false;
		}
	}

	private static final Collection<String> IGNORED_URLS = Arrays.asList(
			"/login", "/static");

	private static boolean isIgnored(String url) {
		for (String ignoreUrl : IGNORED_URLS) {
			if (url.startsWith(StringLib.normalisedUrl(ignoreUrl))) {
				return true;
			}
		}
		return false;
	}
}
