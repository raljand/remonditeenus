package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import view.Form;
import view.View;
import view.field.HasError;
import view.field.model.FieldPair;
import view.field.model.ModelValue;
import app.Function2;
import app.libs.ListLib;
import app.libs.StringLib;
import db.ModelLib;
import db.dao.DaoLib;

public class ResponseLib {

	private ResponseLib() {
	}

	public static void doPost(String successUrl, HttpServletRequest req,
			HttpServletResponse resp, Form<?> form) throws ServletException,
			IOException {
		HasError<? extends List<?>> values = RequestLib.parse(req, form);
		if (values.hasError) {
			forward(View.form(Form.copy(form, ListLib.map(
					new Function2<FieldPair<?, ?>, Object, FieldPair<?, ?>>() {
						@Override
						public FieldPair<?, ?> apply(FieldPair<?, ?> arg0,
								Object arg1) {
							return FieldPair.copy(arg0, arg1);
						}
					}, form.fields, values.value))), req, resp);
		} else {
			ModelLib.setFields(form.model.model, ListLib.map(
					new Function2<FieldPair<?, ?>, Object, ModelValue<?>>() {
						@Override
						public ModelValue<?> apply(FieldPair<?, ?> arg0,
								Object arg1) {
							return ModelValue.saveable(arg0, arg1);
						}
					}, form.fields, values.value));
			DaoLib.save(form.model.model);
			resp.sendRedirect(StringLib.normalisedUrl(successUrl));
		}
	}

	public static void forward(View view, HttpServletRequest req,
			HttpServletResponse resp) throws ServletException, IOException {
		req.setAttribute("view", view);
		req.getRequestDispatcher("/jsp/view.jsp").forward(req, resp);
	}

	public static void error(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.getRequestDispatcher("/html/error.html").forward(req, resp);
	}
}
