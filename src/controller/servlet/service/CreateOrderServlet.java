package controller.servlet.service;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import view.Form;
import app.Function2;
import controller.RequestLib;
import db.model.ServiceOrder;
import db.model.ServiceRequest;

@WebServlet(CreateOrderServlet.URL)
public class CreateOrderServlet extends CreateServlet<ServiceOrder> {

	private static final long serialVersionUID = 1L;

	public static final String URL = "/order/register";
	public static final String REQUEST_PARAM = "request";

	public CreateOrderServlet() {
		super(
				new Function2<HttpServletRequest, HttpServletResponse, Form<ServiceOrder>>() {
					@Override
					public Form<ServiceOrder> apply(HttpServletRequest arg0,
							HttpServletResponse arg1) {
						ServiceRequest request = RequestLib.model(arg0,
								ServiceRequest.class, REQUEST_PARAM);
						return request != null ? Form.orderForm(request, null)
								: null;
					}
				}, "/order");
	}
}
