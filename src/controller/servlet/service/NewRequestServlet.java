package controller.servlet.service;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import view.Form;
import app.Function2;
import db.model.ServiceRequest;

@WebServlet("/service/request/register")
public class NewRequestServlet extends CreateServlet<ServiceRequest> {

	private static final long serialVersionUID = 1L;

	public NewRequestServlet() {
		super(
				new Function2<HttpServletRequest, HttpServletResponse, Form<ServiceRequest>>() {
					@Override
					public Form<ServiceRequest> apply(HttpServletRequest arg0,
							HttpServletResponse arg1) {
						return Form.serviceRequest(arg0, null, null);
					}
				}, RequestServlet.URL);
	}
}
