package controller.servlet.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import view.Form;
import view.FormModel;
import view.ViewRendererLib;
import app.Function2;
import controller.ResponseLib;
import db.ModelLib;

public class CreateServlet<T> extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public final Function2<HttpServletRequest, HttpServletResponse, Form<T>> formFun;
	public final String successUrl;

	public CreateServlet(
			Function2<HttpServletRequest, HttpServletResponse, Form<T>> formFun,
			String successUrl) {
		this.formFun = formFun;
		this.successUrl = successUrl;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Form<T> form = formFun.apply(req, resp);
		if (form == null) {
			resp.sendError(400);
		} else {
			ResponseLib.forward(new view.View(ViewRendererLib.form(form)), req,
					resp);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Form<T> form = formFun.apply(req, resp);
		ResponseLib.doPost(
				successUrl,
				req,
				resp,
				Form.copy(
						form,
						FormModel.copy(form.model,
								ModelLib.newTyped(form.model.model.type))));
	}
}
