package controller.servlet.service;

import java.util.Arrays;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import view.Form;
import app.Function3;

import com.google.common.base.Function;

import db.model.ServiceOrder;

@WebServlet(OrderServlet.URL)
public class OrderServlet extends UpdateServlet<ServiceOrder> {

	private static final long serialVersionUID = 1L;

	public static final String URL = "/order";

	public OrderServlet() {
		super(
				ServiceOrder.class,
				new Function3<HttpServletRequest, HttpServletResponse, ServiceOrder, Form<?>>() {
					@Override
					public Form<?> apply(HttpServletRequest arg0,
							HttpServletResponse arg1, ServiceOrder arg2) {
						return Form.orderForm(arg2.getService_request_fk(),
								arg2);
					}
				}, new Function<ServiceOrder, List<String>>() {
					@Override
					public List<String> apply(ServiceOrder arg0) {
						return Arrays.asList(arg0.getSo_status_type_fk()
								.getType_name(), arg0.getNote());
					}
				}, URL, "Service orders");
	}
}
