package controller.servlet.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import view.Form;
import view.View;
import view.ViewRendererLib;
import app.Function3;

import com.google.common.base.Function;

import controller.RequestLib;
import controller.ResponseLib;
import db.Typed;

public class UpdateServlet<T> extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static final String ID_PARAM = "id";

	private final Class<T> modelType;
	private final Function3<HttpServletRequest, HttpServletResponse, T, Form<?>> formFunc;
	private final Function<T, List<String>> renderFunc;
	private final String url;
	private final String caption;

	public UpdateServlet(
			Class<T> modelType,
			Function3<HttpServletRequest, HttpServletResponse, T, Form<?>> formFunc,
			Function<T, List<String>> renderFunc, String url, String caption) {
		this.modelType = modelType;
		this.formFunc = formFunc;
		this.renderFunc = renderFunc;
		this.url = url;
		this.caption = caption;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		ResponseLib.forward(
				new View(ViewRendererLib.model(
						formFunc,
						ID_PARAM,
						renderFunc,
						req,
						resp,
						new Typed<>(modelType, RequestLib.model(req, modelType,
								ID_PARAM)), caption)), req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		T model = RequestLib.model(req, modelType, ID_PARAM);
		if (model == null) {
			resp.sendError(400);
		} else {
			ResponseLib
					.doPost(url, req, resp, formFunc.apply(req, resp, model));
		}
	}
}
