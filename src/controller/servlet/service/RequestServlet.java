package controller.servlet.service;

import java.util.Arrays;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import view.Form;
import app.Function3;

import com.google.common.base.Function;

import db.model.ServiceRequest;

@WebServlet(RequestServlet.URL)
public class RequestServlet extends UpdateServlet<ServiceRequest> {

	private static final long serialVersionUID = 1L;

	public static final String URL = "/service/request";

	public RequestServlet() {
		super(
				ServiceRequest.class,
				new Function3<HttpServletRequest, HttpServletResponse, ServiceRequest, Form<?>>() {
					@Override
					public Form<?> apply(HttpServletRequest arg0,
							HttpServletResponse arg1, ServiceRequest arg2) {
						return Form.serviceRequest(arg0, arg2, ID_PARAM);
					}
				}, new Function<ServiceRequest, List<String>>() {
					@Override
					public List<String> apply(ServiceRequest arg0) {
						return Arrays.<String> asList(arg0.getCustomer()
								.getSubject().getName(), arg0.getStatus()
								.getType_name(), arg0.getCreated().toString());
					}
				}, URL, "Service Requests");
	}
}
