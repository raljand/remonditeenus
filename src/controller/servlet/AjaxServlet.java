package controller.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.Settings;

public class AjaxServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected final void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String ajaxHeader = req.getHeader("X-Requested-With");
		if (ajaxHeader != null && ajaxHeader.equals("XMLHttpRequest")) {
			doAjax(req, resp);
		} else {
			normalGet(req, resp);
		}
	}

	protected void normalGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (Settings.DEBUG) {
			doAjax(request, response);
		} else {
			response.sendError(405);
		}
	}

	protected void doAjax(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendError(405);
	}
}
