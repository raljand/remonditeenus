package controller.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.dao.DaoLib;
import db.model.Device;

public class DeviceServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Device device = DaoLib.load(Device.class, 1);
		ServletOutputStream outputStream = resp.getOutputStream();
		outputStream.print(device.toString());
		outputStream.flush();
	}
}
