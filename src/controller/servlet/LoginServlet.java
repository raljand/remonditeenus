package controller.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import view.Form;
import view.View;
import view.ViewRendererLib;
import view.field.Field;
import view.field.rendering.RenderData;
import view.field.rendering.Tag;
import app.Settings;
import controller.RequestLib;
import controller.ResponseLib;
import db.dao.UserAccountDAO;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		ResponseLib.forward(
				new View(ViewRendererLib.form(
						Tag.p("kasutaja: juhan<br/>parool: juurikas34metsX"),
						Form.login())), req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Form<?> form = Form.login();
		RenderData<String> username = RequestLib.value(req,
				(Field<RenderData<String>>) form.fields.get(0).formField, null);
		RenderData<String> password = RequestLib.value(req,
				(Field<RenderData<String>>) form.fields.get(1).formField, null);
		Integer id = UserAccountDAO.getId(username.object.value,
				password.object.value);
		if (id == null) {
			ResponseLib.forward(new View(ViewRendererLib.form(Tag
					.p("Parool või kasutajanimi oli vale:<br/>"
							+ "kasutaja: juhan<br/>"
							+ "parool: juurikas34metsX"), form)), req, resp);
		} else {
			HttpSession session = req.getSession();
			session.setAttribute("user", id);
			resp.sendRedirect(Settings.URL_ROOT);
		}
	}
}
