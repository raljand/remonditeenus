package controller.servlet;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import view.SearchAPI;
import view.ViewLib;
import app.libs.ListLib;

import com.google.common.base.Function;

import db.ModelLib;
import db.Typed;
import db.dao.CustomerDAO;
import db.model.Customer;

@WebServlet("/customer/search")
public class CustomerSearchServlet extends AjaxServlet {
	private static final long serialVersionUID = 1L;

	public CustomerSearchServlet() {
		super();
	}

	@Override
	protected void doAjax(HttpServletRequest request,
	        HttpServletResponse response) throws IOException {
		String term = request.getParameter("term");
		if (term == null) {
			term = "";
		}

		Collection<Customer> customers = CustomerDAO.findByName(term);
		ViewLib.writeJson(ListLib.map(new Function<Customer, SearchAPI>() {
			@Override
			public SearchAPI apply(Customer arg0) {
				return new SearchAPI(arg0.getId(), ModelLib.slug(new Typed<>(
				        Customer.class, arg0)));
			}
		},
		        customers), response);
	}
}
