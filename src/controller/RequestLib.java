package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import view.Form;
import view.field.Field;
import view.field.HasError;
import view.field.model.ActionLink;
import view.field.model.FieldPair;

import com.google.common.base.Function;

import db.dao.DaoLib;
import db.dao.UserAccountDAO;
import db.model.Subject;
import db.model.UserAccount;

public class RequestLib {

	public static HasError<? extends List<?>> parse(
			final HttpServletRequest req, final Form<?> form) {
		return HasError.map(new Function<FieldPair<?, ?>, HasError<?>>() {
			@Override
			public HasError<?> apply(FieldPair<?, ?> pair) {
				return HasError.parse(req, form.model.model.value,
						pair.formField);
			}
		}, form.fields);
	}

	public static <T> boolean tryAction(List<ActionLink<T>> actions,
			HttpServletRequest req, String idParam) throws IOException {
		String sAction = req.getParameter(ActionLink.KEYWORD);
		if (sAction != null) {
			for (ActionLink<?> action : actions) {
				if (tryAction(req, action, sAction, idParam)) {
					return true;
				}
			}
		}
		return false;
	}

	private static <T> boolean tryAction(HttpServletRequest req,
			ActionLink<T> link, String sAction, String idParam)
			throws IOException {
		T modelObject = model(req, link.model, idParam);
		if (modelObject != null) {
			if (sAction.equals(link.name)) {
				link.action.update(modelObject);
				return true;
			}
		}
		return false;
	}

	public static <T extends Subject> T getSubject(HttpSession session) {
		Object id = session.getAttribute("user");
		if (id instanceof Integer) {
			UserAccount account = DaoLib.load(UserAccount.class, (Integer) id);
			return UserAccountDAO.subject(account);
		}
		return null;
	}

	public static <T> T model(HttpServletRequest request, Class<T> model,
			String name) {
		Integer id = number(request, name);
		return id != null ? DaoLib.<T> load(model, id) : null;
	}

	public static Integer number(HttpServletRequest request, String name) {
		try {
			String number = request.getParameter(name);
			return Integer.valueOf(number);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public static <T, V> V value(HttpServletRequest req, Field<V> field,
			Object model) {
		return field.parser.parse(req, field.name, model).value;
	}
}
